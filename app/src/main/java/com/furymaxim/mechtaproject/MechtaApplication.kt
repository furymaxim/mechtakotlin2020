package com.furymaxim.mechtaproject

import android.app.Application
import com.furymaxim.mechtaproject.utils.volley.MyVolley.Companion.init
import com.google.firebase.FirebaseApp

class MechtaApplication: Application() {


    override fun onCreate() {
        super.onCreate()

        init(this)
        FirebaseApp.initializeApp(this)
    }
}