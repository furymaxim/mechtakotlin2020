package com.furymaxim.mechtaproject.utils.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.furymaxim.mechtaproject.api.response_models.*
import com.furymaxim.mechtaproject.utils.database.MechtaDatabase.Companion.DATABASE_VERSION
import com.furymaxim.mechtaproject.utils.local.*

@Database(entities = arrayOf(NotificationsRestModel::class, ServicesRestModel::class, CommercialsRestModel::class, ExcursionsRestModel::class, NewsRestModel::class, OffersRestModel::class),version = DATABASE_VERSION ,exportSchema = false)
abstract class MechtaDatabase: RoomDatabase(){
    abstract fun notificationsDAO(): NotificationsDAO
    abstract fun servicesDAO(): ServicesDAO
    abstract fun commercialsDAO(): CommercialsDAO
    abstract fun excursionsDAO(): ExcursionsDAO
    abstract fun newsDAO(): NewsDAO
    abstract fun offersDAO(): OffersDAO


    companion object{
        const val DATABASE_VERSION = 3
        const val DATABASE_NAME = "MechtaDB"

        private var mInstance: MechtaDatabase?= null

        fun getInstance(context: Context): MechtaDatabase {
            if(mInstance == null)
                mInstance = Room.databaseBuilder(context, MechtaDatabase::class.java, DATABASE_NAME)
                    .fallbackToDestructiveMigration()
                    .build()

            return mInstance!!
        }
    }

}