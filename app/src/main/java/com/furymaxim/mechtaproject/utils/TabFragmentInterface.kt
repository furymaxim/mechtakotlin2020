package com.furymaxim.mechtaproject.utils

interface TabFragmentInterface {
    fun fragmentBecameVisible()
}