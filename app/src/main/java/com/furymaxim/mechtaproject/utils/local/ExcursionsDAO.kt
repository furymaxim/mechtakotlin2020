package com.furymaxim.mechtaproject.utils.local

import androidx.room.*
import com.furymaxim.mechtaproject.api.response_models.ExcursionsRestModel
import io.reactivex.Flowable

@Dao
interface ExcursionsDAO {

    @Query("SELECT * FROM ExcursionsRestModel ORDER BY id DESC")
    fun getAllExcursions(): Flowable<List<ExcursionsRestModel?>?>?

    @Delete
    fun deleteAllExcursions(excursionsList: List<ExcursionsRestModel?>?)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveAllExcursions(excursionsList: List<ExcursionsRestModel?>?): Array<Long?>?


    //    @Query("SELECT * FROM History WHERE id = :id")
//    GetTransactionsResponse.History getHistoryById(String id);
//    @Update
//    void updateHistory(List<GetTransactionsResponse.History> historyList);
}