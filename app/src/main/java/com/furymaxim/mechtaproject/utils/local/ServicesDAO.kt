package com.furymaxim.mechtaproject.utils.local

import androidx.room.*
import com.furymaxim.mechtaproject.api.response_models.ServicesRestModel
import io.reactivex.Flowable

@Dao
interface ServicesDAO {

    @Query("SELECT * FROM ServicesRestModel ORDER BY id DESC")
    fun getAllServices(): Flowable<List<ServicesRestModel?>?>?

    @Delete
    fun deleteAllServices(servicesList: List<ServicesRestModel?>?)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveAllServices(servicesList: List<ServicesRestModel?>?): Array<Long?>?
}