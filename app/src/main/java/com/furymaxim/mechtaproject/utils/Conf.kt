package com.furymaxim.mechtaproject.utils

import android.app.Activity
import android.content.Context

class Conf {

    companion object {
        const val URL_MAIN = "https://dream.yii.website"
        const val URL_API = "/api/v1/"
        const val TIME_OUT = 10
        const val TIMESTAMP_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        const val URL_NEWS = URL_MAIN + URL_API + "news"
        const val URL_EXCURSIONS = URL_MAIN + URL_API + "excursions"
        const val URL_SERVICES = URL_MAIN + URL_API + "services"
        const val URL_COMMERCIAL = URL_MAIN + URL_API + "commercial_ads"
        const val URL_OFFERS = URL_MAIN + URL_API + "offers"
        const val URL_BUS_STOPS = URL_MAIN + URL_API + "bus_stops"
        const val URL_BUS_ROUTES = URL_MAIN + URL_API + "bus_routes"
        const val URL_BUS_CHANNELS =
            URL_MAIN + URL_API + "buses/channels?format=json"
        const val URL_NOTIFICATIONS = URL_MAIN + URL_API + "notifications"
        //public static final String URL_GCM_SERVER = "http://77.222.43.53/gcm-notifications/device/";
        const val URL_FCM_SERVER = "https://dream-push.herokuapp.com/gcm-notifications/device/"
        const val URL_SERVICES_INT = "internal"
        const val URL_SERVICES_EXT = "external"
        private const val STOP_SHARED = "STOP_SHARED"

        fun getLastStopA(context: Context): Int {
            val preferences =
                context.getSharedPreferences(STOP_SHARED, Activity.MODE_PRIVATE)
            return preferences.getInt("LAST_A", 5)
        }

        fun getLastStopB(context: Context): Int {
            val preferences =
                context.getSharedPreferences(STOP_SHARED, Activity.MODE_PRIVATE)
            return preferences.getInt("LAST_B", 10)
        }

        fun saveLastStopA(context: Context, id: Int) {
            context.getSharedPreferences(STOP_SHARED, Activity.MODE_PRIVATE).edit()
                .putInt("LAST_A", id).apply()
        }

        fun saveLastStopB(context: Context, id: Int) {
            context.getSharedPreferences(STOP_SHARED, Activity.MODE_PRIVATE).edit()
                .putInt("LAST_B", id).apply()
        }
    }
}