package com.furymaxim.mechtaproject.utils.volley

import android.content.Context
import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley
import java.io.UnsupportedEncodingException
import java.nio.charset.Charset

class MyVolley {

    companion object {
        var mRequestQueue: RequestQueue? = null

        fun init(context: Context?) {
            mRequestQueue = Volley.newRequestQueue(context)
        }

        fun getRequestQueue():RequestQueue?{
            if (mRequestQueue != null) {
                return mRequestQueue
            } else {
                throw IllegalStateException("RequestQueue not initialized")
            }
        }

        fun getCache(url: String?): String {
            val cache = getRequestQueue()!!.cache
            val entry = cache[url]
            return when(entry){
                null -> ""
                else -> try {
                    return String(entry.data, Charset.forName("UTF-8"))
                } catch (e: UnsupportedEncodingException) {
                    return ""
                }
            }
        }

    }


}