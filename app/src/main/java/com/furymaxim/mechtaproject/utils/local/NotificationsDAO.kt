package com.furymaxim.mechtaproject.utils.local

import androidx.room.*
import com.furymaxim.mechtaproject.api.response_models.NotificationsRestModel
import io.reactivex.Flowable

@Dao
interface NotificationsDAO {

    @Query("SELECT * FROM NotificationsRestModel ORDER BY published_at DESC")
    fun getAllNotifications(): Flowable<List<NotificationsRestModel?>?>?

    @Query("SELECT * FROM NotificationsRestModel WHERE push_type IN (:types) ORDER BY published_at DESC")
    fun getFilteredNotifications(types: List<String?>?): Flowable<List<NotificationsRestModel?>?>?

    @Delete
    fun deleteAllNotifications(notificationsList: List<NotificationsRestModel?>?)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveAllNotifications(notificationsList: List<NotificationsRestModel?>?): Array<Long?>?
}