package com.furymaxim.mechtaproject.utils.local

import androidx.room.*
import com.furymaxim.mechtaproject.api.response_models.CommercialsRestModel
import io.reactivex.Flowable

@Dao
interface CommercialsDAO {

    @Query("SELECT * FROM CommercialsRestModel ORDER BY id DESC")
    fun getAllCommercials(): Flowable<List<CommercialsRestModel?>?>?

    @Delete
    fun deleteAllCommercials(commercialList: List<CommercialsRestModel?>?)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveAllCommercials(commercialList: List<CommercialsRestModel?>?): Array<Long?>?
}