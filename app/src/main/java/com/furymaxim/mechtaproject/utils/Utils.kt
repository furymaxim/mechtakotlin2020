package com.furymaxim.mechtaproject.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.preference.PreferenceManager
import android.view.inputmethod.InputMethodManager
import androidx.annotation.NonNull
import androidx.fragment.app.FragmentActivity
import com.furymaxim.mechtaproject.models.SettingsModel
import com.furymaxim.mechtaproject.ui.activities.MainActivity
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken


class Utils {

    companion object{

        fun hideKeyboard(@NonNull activity: Activity){
            val view = activity.currentFocus
            if (view != null) {
                val inputManager: InputMethodManager =
                    activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                inputManager.hideSoftInputFromWindow(
                    view.windowToken,
                    InputMethodManager.HIDE_NOT_ALWAYS
                )
            }
        }

        fun setActionBarTitle(title: String, fragmentActivity: FragmentActivity){
            val actionBar = (fragmentActivity as MainActivity).supportActionBar

            actionBar!!.title = title
        }

        fun getSettings(context: Context?): MutableList<SettingsModel?>? {
            return if (PreferenceManager.getDefaultSharedPreferences(context).getString(
                    "Settings",
                    ""
                ) != ""
            ) {
                Gson().fromJson(
                    PreferenceManager.getDefaultSharedPreferences(context).getString(
                        "Settings",
                        ""
                    ), object : TypeToken<List<SettingsModel?>?>() {}.type
                )
            } else {
                null
            }
        }

        fun setSettings(context: Context?, settingsList: MutableList<SettingsModel?>?) {
            PreferenceManager.getDefaultSharedPreferences(context).edit()
                .putString("Settings", Gson().toJson(settingsList)).apply()
        }

        fun openURL(url: String): Intent? {

            val modifiedUrl: String?

            if (!url.startsWith("http://") && !url.startsWith("https://")) modifiedUrl = "http://$url" else modifiedUrl = url

            return Intent(Intent.ACTION_VIEW, Uri.parse(modifiedUrl))
        }


    }
}