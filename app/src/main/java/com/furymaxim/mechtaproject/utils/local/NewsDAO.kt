package com.furymaxim.mechtaproject.utils.local

import androidx.room.*
import com.furymaxim.mechtaproject.api.response_models.NewsRestModel
import io.reactivex.Flowable

@Dao
interface NewsDAO {

    @Query("SELECT * FROM NewsRestModel ORDER BY updatedAt DESC")
    fun getAllNews(): Flowable<List<NewsRestModel?>?>?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveAllNews(newsList: List<NewsRestModel?>?): Array<Long?>?

    @Delete
    fun deleteNews(historyList: List<NewsRestModel?>?)

    @Query("DELETE FROM NewsRestModel")
    fun deleteAllNews() //    @Query("SELECT * FROM History WHERE id = :id")
//    GetTransactionsResponse.History getHistoryById(String id);
//    @Update
//    void updateHistory(List<GetTransactionsResponse.History> historyList);
}