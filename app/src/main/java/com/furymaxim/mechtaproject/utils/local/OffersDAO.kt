package com.furymaxim.mechtaproject.utils.local

import androidx.room.*
import com.furymaxim.mechtaproject.api.response_models.OffersRestModel
import io.reactivex.Flowable

@Dao
interface OffersDAO {

    @Query("SELECT * FROM OffersRestModel ORDER BY updatedAt DESC")
    fun  getAllOffers(): Flowable<List<OffersRestModel?>?>?

    @Delete
    fun deleteAllOffers(offersList: List<OffersRestModel?>?)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveAllOffers(offersList: List<OffersRestModel?>?): Array<Long?>?

    //    @Query("SELECT * FROM History WHERE id = :id")
//    GetTransactionsResponse.History getHistoryById(String id);
//    @Update
//    void updateHistory(List<GetTransactionsResponse.History> historyList);
}