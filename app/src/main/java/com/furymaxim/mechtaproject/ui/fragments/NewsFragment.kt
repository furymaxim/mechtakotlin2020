package com.furymaxim.mechtaproject.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.furymaxim.mechtaproject.R
import com.furymaxim.mechtaproject.ui.activities.MainActivity
import com.furymaxim.mechtaproject.ui.fragments.tabs.news_tabs.ExcursionsTabFragment
import com.furymaxim.mechtaproject.ui.fragments.tabs.news_tabs.NewsTabFragment
import com.furymaxim.mechtaproject.ui.fragments.tabs.news_tabs.OffersTabFragment
import com.furymaxim.mechtaproject.utils.Utils
import com.google.android.material.tabs.TabLayout

class NewsFragment : BaseFragment() {

    var mTitles = arrayOfNulls<String>(3)
    private var pager: ViewPager? = null
    private var lastSelect = 0
    private var tabs: TabLayout? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_news, container, false)

        Utils.setActionBarTitle(getString(R.string.drawer_item_news),activity!!)

        (activity as MainActivity).showFlower(true)

        mTitles = resources.getStringArray(R.array.news_tabs)

        val adapter = PagerAdapter(childFragmentManager)

        pager = view.findViewById(R.id.pager)
        tabs = view.findViewById(R.id.tabs)

        pager!!.adapter = adapter
        setupTabLayout()
        pager!!.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
            }

            override fun onPageSelected(position: Int) {
                update(position)
            }

            override fun onPageScrollStateChanged(state: Int) {}
        })
        pager!!.currentItem = lastSelect

        return view
    }

    private fun update(position: Int) {
        for (i in mTitles.indices) {

            tabs!!.getTabAt(i)!!.customView!!.findViewById<TextView>(R.id.text).isSelected = false

            if (i == position) tabs!!.getTabAt(i)!!.customView!!.findViewById<TextView>(R.id.text).isSelected = true

        }
        lastSelect = position
    }

    private fun setupTabLayout(){
        for (current in mTitles.indices) {
            val tab: TabLayout.Tab = tabs!!.newTab()

            tab.setCustomView(R.layout.tabs)

            (tab.customView as FrameLayout).findViewById<TextView>(R.id.text).text = mTitles[current]

            tab.customView!!.setOnClickListener { pager!!.currentItem = current }

            tabs!!.addTab(tab)
        }
    }

    override fun onResume() {
        super.onResume()
        update(pager!!.currentItem)
    }

    inner class PagerAdapter(fm: FragmentManager?) : FragmentPagerAdapter(fm!!, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

        override fun getPageTitle(position: Int): CharSequence? {
            return mTitles[position]
        }

        override fun getItem(position: Int): Fragment {
            return when (position) {
                1 -> ExcursionsTabFragment()
                2 -> OffersTabFragment()
                else -> NewsTabFragment()
            }
        }

        override fun getCount(): Int {
            return mTitles.size
        }
    }

    companion object{

        fun newInstance(): NewsFragment {

            return NewsFragment()
        }
    }
}