package com.furymaxim.mechtaproject.ui.navigation_drawer;

import android.content.Context;
import android.view.View;

import androidx.annotation.LayoutRes;
import androidx.core.content.ContextCompat;

import com.furymaxim.mechtaproject.R;
import com.mikepenz.fastadapter.utils.ViewHolderFactory;
import com.mikepenz.materialdrawer.model.AbstractBadgeableDrawerItem;
import com.mikepenz.materialdrawer.model.BaseViewHolder;


public class MenuDrawerItem extends AbstractBadgeableDrawerItem<MenuDrawerItem> {

    private int lineColor;

    @Override
    public ViewHolderFactory<AbstractBadgeableDrawerItem.ViewHolder> getFactory() {
        return new ItemFactory();
    }

    @Override
    protected void bindViewHelper(BaseViewHolder viewHolder) {
        super.bindViewHelper(viewHolder);
        if (lineColor != 0) {
            ((ViewHolder) viewHolder).line.setBackgroundColor(lineColor);
            ((ViewHolder) viewHolder).itemView.setPadding(0, 0, 0, 0);
        }
    }

    @Override
    @LayoutRes
    public int getLayoutRes() {
        return R.layout.menu_drawer_item;
    }

    public MenuDrawerItem withLineColor(Context context, int id) {
        lineColor = ContextCompat.getColor(context, id);
        return this;
    }

    public static class ItemFactory extends AbstractBadgeableDrawerItem.ItemFactory {

        @Override
        public ViewHolder create(View v) {
            return new ViewHolder(v);
        }
    }

    private static class ViewHolder extends AbstractBadgeableDrawerItem.ViewHolder {
        private View badgeContainer;
        private View line;

        ViewHolder(View view) {
            super(view);
            this.line = view.findViewById(R.id.line);
            this.badgeContainer = view.findViewById(com.mikepenz.materialdrawer.R.id.material_drawer_badge_container);
        }
    }


}
