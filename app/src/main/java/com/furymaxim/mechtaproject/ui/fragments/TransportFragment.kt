package com.furymaxim.mechtaproject.ui.fragments


import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.*
import android.widget.*
import android.widget.AdapterView.OnItemSelectedListener
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.furymaxim.mechtaproject.R
import com.furymaxim.mechtaproject.adapters.SpinnerTransportAdapter
import com.furymaxim.mechtaproject.api.response_models.BusRoutesRestModel
import com.furymaxim.mechtaproject.api.response_models.BusStopsRestModel
import com.furymaxim.mechtaproject.api.response_models.NotificationsRestModel
import com.furymaxim.mechtaproject.models.NearestBusModel
import com.furymaxim.mechtaproject.models.ScheduleMinutesModel
import com.furymaxim.mechtaproject.models.ScheduleModel
import com.furymaxim.mechtaproject.ui.activities.MainActivity
import com.furymaxim.mechtaproject.ui.fragments.tabs.transport_tabs.NearestBusTabFragment
import com.furymaxim.mechtaproject.ui.fragments.tabs.transport_tabs.ScheduleTabFragment
import com.furymaxim.mechtaproject.utils.Conf
import com.furymaxim.mechtaproject.utils.TabFragmentInterface
import com.furymaxim.mechtaproject.utils.Utils
import com.furymaxim.mechtaproject.utils.volley.MyVolley
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback

import com.google.android.gms.maps.model.BitmapDescriptorFactory.fromResource
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import kotlinx.android.synthetic.main.fragment_transport.*
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.Collections.sort
import kotlin.collections.ArrayList

class TransportFragment : BaseFragment(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    private lateinit var map: GoogleMap
    var mTitles = arrayOfNulls<String>(2)
    private var menu: Menu? = null
    private var mInflater: LayoutInflater? = null
    private var myMarker: Marker ?=null
    private var markerA: Marker ?=null
    private var markerB: Marker? = null

    private var mRequestBusStops: StringRequest? = null
    private var mRequestBusRoutes: StringRequest? = null
    private var busStopsList: List<BusStopsRestModel?>? = null
    private var busRoutesList: List<BusRoutesRestModel?>? = null

    private var nearestBusModelsList: ArrayList<NearestBusModel?>? = null

    private val fragmentA: Fragment = NearestBusTabFragment()
    private val fragmentB: Fragment = ScheduleTabFragment()

    private var idA: Long? = null
    private var idB: Long? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_transport, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mInflater = layoutInflater

        Utils.setActionBarTitle(getString(R.string.drawer_item_transport), activity!!)

        (activity as MainActivity).showFlower(false)

        mTitles = resources.getStringArray(R.array.transport_tabs)

        val adapter = PagerAdapter(childFragmentManager)

        pager.adapter = adapter
        tabs.setupWithViewPager(pager)
        pager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}

            override fun onPageSelected(position: Int) {
                val fragment = adapter.instantiateItem(pager, position) as TabFragmentInterface

                fragment.fragmentBecameVisible()

            }

            override fun onPageScrollStateChanged(state: Int) {}
        })

        reverse.setOnClickListener {
            if(spinnerA.selectedItemPosition == 0 || spinnerB.selectedItemPosition == 0){
                Toast.makeText(context,"Должны быть указаны обе остановки",Toast.LENGTH_SHORT).show()
            } else {
            val temp: Int = spinnerA!!.selectedItemPosition
            spinnerA!!.setSelection(spinnerB!!.selectedItemPosition)
            spinnerB!!.setSelection(temp)
            if (mapView.visibility == View.VISIBLE) {
                  map.clear()

                  addSelectedMarker("a", LatLng(busStopsList!![spinnerA.selectedItemPosition - 1]!!.latitude!!.toDouble(), busStopsList!![spinnerA.selectedItemPosition - 1]!!.longitude!!.toDouble()), 9f,busStopsList!![spinnerA.selectedItemPosition - 1]!!.title!!)
                  addSelectedMarker("b", LatLng(busStopsList!![spinnerB.selectedItemPosition - 1]!!.latitude!!.toDouble(), busStopsList!![spinnerB.selectedItemPosition - 1]!!.longitude!!.toDouble()), 9f,busStopsList!![spinnerB.selectedItemPosition - 1]!!.title!!)
                    }
               // }
          //  }
            }
        }


        stopsPosition.setOnClickListener {
            if(mapView.visibility == View.GONE){
                //stopsLayout.visibility = View.GONE
                if(networkAvailable()) {
                    mapView.visibility = View.VISIBLE
                    buttonLayout.visibility = View.GONE
                    spinnerLayout.visibility = View.GONE
                    map.clear()
                    addMultipleMarkers()
                    map.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(55.75222, 37.61556), 9f))
                    menu!!.findItem(R.id.buttonMap).isVisible = false
                    menu!!.findItem(R.id.buttonCheck).isVisible = false

                    val params = stopsLayout!!.layoutParams as RelativeLayout.LayoutParams
                    params.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0)
                    params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM)
                    params.topMargin = 0
                    stopsLayout!!.layoutParams = params

                    stopsPosition.text = "Закрыть карту"
                    stopsPosition.textSize = 16f
                }else{
                    Toast.makeText(context,R.string.need_conn,Toast.LENGTH_SHORT).show()
                }

            }else{
                map.clear()
                mapView.visibility = View.GONE
                buttonLayout.visibility = View.VISIBLE
                spinnerLayout.visibility = View.VISIBLE
                menu!!.findItem(R.id.buttonMap).isVisible = true
                menu!!.findItem(R.id.buttonCheck).isVisible = false
                //stopsLayout.visibility = View.VISIBLE

                val params = stopsLayout!!.layoutParams as RelativeLayout.LayoutParams
                params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, 0)
                params.addRule(RelativeLayout.ALIGN_PARENT_TOP)
                stopsLayout.layoutParams = params

                stopsPosition.text = "Посмотреть расположение всех остановок"
                stopsPosition.textSize = 14f
            }
        }
    }



    private fun getBusStopsFromCache(): ArrayList<BusStopsRestModel?>? {
        val s: String = MyVolley.getCache(Conf.URL_BUS_STOPS)
        return if (s.isNotEmpty()) {
            Gson().fromJson(s, BusStopsRestModel.Type)
        } else ArrayList()
    }

    private fun getBusRoutesFromCache(): ArrayList<BusRoutesRestModel?>? {
        val s: String = MyVolley.getCache(Conf.URL_BUS_ROUTES)
        return if (s.isNotEmpty()) {
            Gson().fromJson(s, BusRoutesRestModel.Type)
        } else ArrayList()
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

    }

    override fun onMapReady(map: GoogleMap?) {

        map?.let { this.map = it }

        //map!!.uiSettings.isZoomControlsEnabled = true
        setDefaultZoomPreferences()
        map!!.setOnMarkerClickListener(this)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mapView.onCreate(savedInstanceState)
        mapView.onResume()

        mapView.getMapAsync(this)
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {

        this.menu = menu

        menu.findItem(R.id.buttonMap).isVisible = true
        menu.findItem(R.id.buttonCheck).isVisible = false

        setMenuItems(menu.findItem(R.id.buttonCheck), menu.findItem(R.id.buttonMap))
        super.onCreateOptionsMenu(menu, inflater)
    }

  /*  private fun createMarker(latitude: Double, longitude: Double, iconResID: Int): Marker {

        return map.addMarker(MarkerOptions()
            .position(LatLng(latitude,longitude))
            .anchor(0.5f, 0.5f)
            .icon(fromResource(iconResID)))
    }

   */

    private fun createMarkerWithTitle(latitude: Double, longitude: Double, iconResID: Int, title: String): Marker {

        myMarker = map.addMarker(MarkerOptions()
            .position(LatLng(latitude,longitude))
            .anchor(0.5f, 0.5f)
            .title(title)
            .icon(fromResource(iconResID)))

        map.setOnMarkerClickListener(this)

        return myMarker!!
    }


    override fun onMarkerClick(marker: Marker?): Boolean {

        if(marker!! == myMarker){
            marker.showInfoWindow()
        }

        /*
      //  if(marker!! == myMarker){
           val title = marker!!.title
        /*  for(i in busStopsList!!.indices){
             if(busStopsList!![i]!!.title == title) {
               //  spinnerA.setSelection((spinnerA.adapter as SpinnerTransportAdapter).getItemId(i+1).toInt())
                // var id = 0*/
                if(spinnerA.selectedItemPosition == 0) {
                    for (item in (spinnerA.adapter as SpinnerTransportAdapter).getItems()!!) {
                        if (item!!.title == title) {
                            val id = (spinnerA.adapter as SpinnerTransportAdapter).getItemId(item.id)
                            //spinnerA.setSelection((spinnerA.adapter as SpinnerTransportAdapter).getItemId(item.id).toInt())
                            for (i in busStopsList!!.indices) {
                                if (busStopsList!![i]!!.id == id.toInt()) {
                                  //  spinnerA.onItemSelectedListener = null
                                    marker.isVisible = false
                                    addSelectedMarker("a", LatLng(item.latitude.toDouble(), item.longitude.toDouble()), 9f)
                                    spinnerA.setSelection(i + 1)
                                }
                            }
                        }

                    }
                }else {
                    if (spinnerB.selectedItemPosition == 0) {
                        for (item in (spinnerB.adapter as SpinnerTransportAdapter).getItems()!!) {
                            if (item!!.title == title) {
                                val id = (spinnerB.adapter as SpinnerTransportAdapter).getItemId(item.id)
                                //spinnerA.setSelection((spinnerA.adapter as SpinnerTransportAdapter).getItemId(item.id).toInt())
                                for (i in busStopsList!!.indices) {
                                    if (busStopsList!![i]!!.id == id.toInt()) {
                                        // spinnerB.onItemSelectedListener = null
                                        marker.isVisible = false
                                        addSelectedMarker("b", LatLng(item.latitude.toDouble(), item.longitude.toDouble()), 9f)
                                        spinnerB.setSelection(i + 1)
                                    }
                                }
                            }

                        }
                    }
                }

            //    }
          //  }
      //  }

        return false

         */
        return false
    }

    private fun addMultipleMarkers(){
            for(marker in busStopsList!!.iterator()) createMarkerWithTitle(marker!!.latitude!!.toDouble(), marker.longitude!!.toDouble(),  R.drawable.ic_comma, marker.title!!)
        //else
      //      for(marker in busStopsList!!.iterator()) createMarker(marker!!.latitude.toDouble(), marker.longitude.toDouble(),  iconResID)
    }


    private fun addSelectedMarker(marker: String?,latLng: LatLng, zoom: Float,title: String): Marker{

        map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,zoom))

        if(marker == "a") {
            markerA = map.addMarker(
                MarkerOptions()
                    .position(latLng)
                    .icon(fromResource(R.drawable.selected_mark)))

           return markerA!!
        }else{
            markerB = map.addMarker(
                MarkerOptions()
                    .position(latLng)
                    .icon(fromResource(R.drawable.selected_mark))
                    .title(title))
            markerB!!.showInfoWindow()
            return markerB!!
        }

    }

    private fun setDefaultZoomPreferences(){
        map.setMinZoomPreference(9f)
        map.setMaxZoomPreference(15f)
    }

    private fun setMenuItems(menuItemCheck: MenuItem, menuItemMap: MenuItem) {
        menuItemMap.setOnMenuItemClickListener {
            if(networkAvailable()) {
                if (spinnerA.selectedItemPosition == 0 || spinnerB.selectedItemPosition == 0) {
                    Toast.makeText(context, "Должны быть указаны обе остановки", Toast.LENGTH_SHORT).show()
                } else {
                    mapView.visibility = View.VISIBLE
                    menuItemMap.isVisible = false
                    menuItemCheck.isVisible = true
                    val params = stopsLayout!!.layoutParams as RelativeLayout.LayoutParams
                    params.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0)
                    params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM)
                    stopsLayout.layoutParams = params //causes layout update
                    layoutPos.visibility = View.GONE

                    /* if (busStopsList!!.isNotEmpty()) {
                    if (spinnerA.selectedItemPosition == 0) {
                        map.clear()
                        addMultipleMarkers(R.drawable.ic_comma, true)
                        map.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(55.75222, 37.61556), 9f))
                    } else {*/
                    map.clear()

                    addSelectedMarker(
                        "a",
                        LatLng(
                            busStopsList!![spinnerA.selectedItemPosition - 1]!!.latitude!!.toDouble(),
                            busStopsList!![spinnerA.selectedItemPosition - 1]!!.longitude!!.toDouble()
                        ),
                        9f,
                        busStopsList!![spinnerA.selectedItemPosition - 1]!!.title!!
                    )
                    addSelectedMarker(
                        "b",
                        LatLng(
                            busStopsList!![spinnerB.selectedItemPosition - 1]!!.latitude!!.toDouble(),
                            busStopsList!![spinnerB.selectedItemPosition - 1]!!.longitude!!.toDouble()
                        ),
                        9f,
                        busStopsList!![spinnerB.selectedItemPosition - 1]!!.title!!
                    )

                    markerB!!.showInfoWindow()

                    // }
                    // }
                }
            }else{
                Toast.makeText(context,R.string.need_conn,Toast.LENGTH_SHORT).show()
            }

            true
        }

        menuItemCheck.setOnMenuItemClickListener {
            mapView.visibility = View.GONE
            menuItemMap.isVisible = true
            menuItemCheck.isVisible = false

            val params = stopsLayout!!.layoutParams as RelativeLayout.LayoutParams
            params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, 0)
            params.addRule(RelativeLayout.ALIGN_PARENT_TOP)
            stopsLayout.layoutParams = params //causes layout update
            layoutPos.visibility = View.VISIBLE
            true
        }
    }

    private fun doRequest() {
        mRequestBusStops = StringRequest(
            Request.Method.GET,
            Conf.URL_BUS_STOPS,
            busStopListener,
            busStopErrorListener
        )
        mRequestBusRoutes = StringRequest(
            Request.Method.GET,
            Conf.URL_BUS_ROUTES,
            busRoutesListener,
            busRoutesErrorListener
        )

        MyVolley.getRequestQueue()!!.add(mRequestBusStops)
        MyVolley.getRequestQueue()!!.add(mRequestBusRoutes)
    }

    override fun onDestroyView() {
        if (menu != null) {
            menu!!.findItem(R.id.buttonMap).isVisible = false
            menu!!.findItem(R.id.buttonCheck).isVisible = false
            menu!!.findItem(R.id.buttonMap).setOnMenuItemClickListener(null)
            menu!!.findItem(R.id.buttonCheck).setOnMenuItemClickListener(null)
        }
        super.onDestroyView()
    }

    private val busRoutesListener: Response.Listener<String?> = Response.Listener { s ->
        if (s != null && s.isNotEmpty()) {
            Log.v("RESPONSE_BUS_ROUTES", s)
            busRoutesList = Gson().fromJson(s, BusRoutesRestModel.Type)
        } else {
            busRoutesList = getBusRoutesFromCache()
            mRequestBusRoutes!!.deliverError(null)
        }
    }
    private val busStopErrorListener = Response.ErrorListener {
        Toast.makeText(context, resources.getString(R.string.need_conn), Toast.LENGTH_LONG).show()
    }
    private val busRoutesErrorListener = Response.ErrorListener {
        Toast.makeText(context, resources.getString(R.string.need_conn), Toast.LENGTH_LONG).show()
    }

    private val busStopListener: Response.Listener<String?> = Response.Listener { s ->
            if (s != null && s.isNotEmpty()) {
                Log.v("RESPONSE_BUS_STOPS", s)

                busStopsList = try {
                    Gson().fromJson(s, BusStopsRestModel.Type)
                } catch (ex: JsonSyntaxException) {
                    if (activity != null) Toast.makeText(activity, resources.getString(R.string.error_server), Toast.LENGTH_SHORT).show()
                    return@Listener
                }
            } else {
                busStopsList = getBusStopsFromCache()
                mRequestBusStops!!.deliverError(null)
            }
            if (activity == null) {
                return@Listener
            }
            val lastA = Conf.getLastStopA(activity!!)
            val lastB = Conf.getLastStopB(activity!!)
            var savePosA = -1
            var savePosB = -1
          //  addMultipleMarkers(R.drawable.ic_comma,true)
            val busStopsModels: MutableList<BusStopsRestModel?>? = ArrayList()
            busStopsModels!!.add(BusStopsRestModel(-1, "Выберите остановку", null, null))
            if (busStopsList != null) busStopsModels.addAll(busStopsList!!.toTypedArray())

            for (i in busStopsModels.indices) {
                if (busStopsModels[i]!!.id == lastA) {
                    savePosA = i
                }
                if (busStopsModels[i]!!.id == lastB) {
                    savePosB = i
                }
            }
            val adapter = SpinnerTransportAdapter(mInflater!!, busStopsModels)
            var spinnerASelected = false
            var spinnerBSelected = false
            val onSpinnerA: OnItemSelectedListener = object : OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>, view: View, pos: Int, id: Long) {
                    try {
                        (parent.getChildAt(0) as TextView).setTextColor(ContextCompat.getColor(context!!,R.color.new_text_dark))
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                    if (id != -1L) {
                        spinnerASelected = true
                        idA = id
                        Conf.saveLastStopA(activity!!, busStopsModels[pos]!!.id)


                       /* val list: List<OverlayItem> =
                            mOverlay.getOverlayItemList()
                        for (i in list.indices) {
                            if (list[i].getBalloonItem().getText().equals(
                                    busStopsModels[pos]!!.getTitle()
                                )
                            ) {
                                mOverlay.setFirstSelected(list[i])
                                break
                            }
                        }
                        if (pos != 0 && mapView.visibility == View.VISIBLE) {
                            mMapController.setPositionNoAnimationTo(list[spinner_a.getSelectedItemPosition() - 1].getGeoPoint())
                            mMapController.setZoomCurrent(13)
                        }*/
                        //map.clear()
                       // myMarker!!.isVisible = false
                        if (spinnerBSelected) {
                            doRoutesRequest()

                            if(mapView.visibility == View.VISIBLE && pos!=0) {
                              //  myMarker!!.isVisible = false
                                markerA!!.isVisible = false
                                addSelectedMarker("a",LatLng(busStopsModels[pos]!!.latitude!!.toDouble(), busStopsModels[pos]!!.longitude!!.toDouble()), 9f,busStopsModels[pos]!!.title!!)
                            }
                       /* }else{
                          //  map.clear()
                            if(mapView.visibility == View.VISIBLE && pos!=0) {
                                addSelectedMarker("a",LatLng(busStopsModels[pos]!!.latitude.toDouble(), busStopsModels[pos]!!.longitude.toDouble()), 9f,busStopsModels[pos]!!.title!!)
                            }
                        }
                    } else {
                        spinnerASelected = false
                       // mOverlay.setFirstSelected(null)*/
                    }
                }else{
                        spinnerASelected = false
                    }
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {}
            }

            val onSpinnerB: OnItemSelectedListener = object : OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>, view: View, pos: Int, id: Long) {
                    try {
                        (parent.getChildAt(0) as TextView).setTextColor(ContextCompat.getColor(context!!,R.color.new_text_dark))
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                    if (id != -1L) {
                        spinnerBSelected = true
                        idB = id
                        Conf.saveLastStopB(activity!!, busStopsModels[pos]!!.id)

                        //  myMarker!!.isVisible = false
                        if (spinnerASelected) {


                            doRoutesRequest()

                            if (mapView.visibility == View.VISIBLE) {
                                //   myMarker!!.isVisible = false
                                markerB!!.isVisible = false
                                addSelectedMarker(
                                    "b",
                                    LatLng(
                                        busStopsModels[pos]!!.latitude!!.toDouble(),
                                        busStopsModels[pos]!!.longitude!!.toDouble()
                                    ),
                                    9f,
                                    busStopsModels[pos]!!.title!!
                                )
                            }
/*                        }else{
                        //    map.clear()
                            if(mapView.visibility == View.VISIBLE && pos!=0) {
                                addSelectedMarker("b",LatLng(busStopsModels[pos]!!.latitude.toDouble(), busStopsModels[pos]!!.longitude.toDouble()), 9f,busStopsModels[pos]!!.title!!)
                            }
                        }
                    } */
                            /*else {
                        spinnerBSelected = false
                      //  mOverlay.setSecondSelected(null)
                    }*/
                        }
                    }else {
                        spinnerBSelected = false
                    }
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {}
            }
            spinnerA!!.adapter = adapter
            spinnerB!!.adapter = adapter
            spinnerA.onItemSelectedListener = onSpinnerA
            spinnerB.onItemSelectedListener = onSpinnerB
            if (savePosA != -1 && savePosB != -1) {
                val handler = Handler(Looper.getMainLooper())
                val finalSavePosA = savePosA
                val finalSavePosB = savePosB
                handler.postDelayed({
                    spinnerA!!.setSelection(finalSavePosA)
                    spinnerB!!.setSelection(finalSavePosB)
                }, 1000)
            }
        }

    private fun doRoutesRequest() {
        nearestBusModelsList = ArrayList()
        val nearestBusModelsToShow: MutableList<NearestBusModel?> = generateNearestBusToShow(false)!!.toMutableList()
        val scheduleModels: MutableList<ScheduleModel?> = ArrayList()
        // * ставим если только в будни доступно
        for (i in nearestBusModelsList!!.indices) {
            try {
                val isWeekend: Boolean = nearestBusModelsList!![i]!!.isWeekend
                // val isWorkdays: Boolean = nearestBusModelsList!![i]!!.isWorkdays
                val time = SimpleDateFormat("HH:mm", Locale("ru","RU")).parse(nearestBusModelsList!![i]!!.from)
                var scheduleMinutesModels: MutableList<ScheduleMinutesModel> = ArrayList()
                var containHour = -1
                for (j in scheduleModels.indices) {
                    if (scheduleModels[j]!!.hour.equals(String.format("%02d", time.hours)
                        )
                    ) {
                        containHour = j
                    }
                }
                val status: Int = nearestBusModelsList!![i]!!.status
                val text: String = nearestBusModelsList!![i]!!.text!!
                if (containHour != -1) { //Добавляем минуты в scheduleModel.get(j)
                    scheduleMinutesModels = scheduleModels[containHour]!!.minutes!!.toMutableList()
                    var minute = String.format("%02d", time.minutes)
                    if (!isWeekend) minute += "*"
                    scheduleMinutesModels.add(ScheduleMinutesModel(minute, status, text))
                    scheduleModels[containHour]!!.minutes = scheduleMinutesModels
                } else { //Добавляем новую модель
                    var minute = String.format("%02d", time.minutes)
                    if (!isWeekend) minute += "*"
                    scheduleMinutesModels.add(ScheduleMinutesModel(minute, status, text))
                    scheduleModels.add(ScheduleModel(String.format("%02d", time.hours), scheduleMinutesModels)
                    )
                }
            } catch (e: KotlinNullPointerException) {
                e.printStackTrace()
            }
        }
        sort(scheduleModels) {
                sm1, sm2 -> sm1!!.hour!!.compareTo(sm2!!.hour!!,true)
        }

        for (scheduleModel in scheduleModels) {
            sort<ScheduleMinutesModel>(scheduleModel!!.minutes!!) {
                    smm1, smm2 -> smm1.minute!!.compareTo(smm2!!.minute!!,true)
            }
        }
        val nearestBusModelsToShowTomorrow: List<NearestBusModel> = generateNearestBusToShow(true)!!
        nearestBusModelsToShow.addAll(nearestBusModelsToShowTomorrow)

        if (nearestBusModelsToShow.isEmpty()) {
            nearestBusModelsToShow.add(NearestBusModel("См. расписание", "", "", -1, "", isWorkdays = false, isWeekend = false))
        }
        if (scheduleModels.isNotEmpty()) scheduleModels.add(0, ScheduleModel())


        (fragmentA as NearestBusTabFragment).showNearestBus(nearestBusModelsToShow)
        (fragmentB as ScheduleTabFragment).showSchedule(scheduleModels)
    }

    private fun generateNearestBusToShow(tomorrow: Boolean): List<NearestBusModel>? {
        val nearestBusModelsToShowTomorrow: MutableList<NearestBusModel> = ArrayList()
        val nearestBusModelsToShow: MutableList<NearestBusModel> = ArrayList()
        if (busRoutesList != null) {
            for (i in busRoutesList!!.indices) {
                val busRoutesModel = busRoutesList!![i]!!
                val notificationsRestModel = busRoutesModel.notifications
                for (j in busRoutesModel.flights!!.indices) {
                    val flightsModel = busRoutesModel.flights!![j]
                    var wasA = false
                    var wasB = false
                    var timeFrom = ""
                    var timeTo = ""
                    for (k in flightsModel.flightBusStopModelList!!.indices) {
                        val flightBusStopModel = flightsModel.flightBusStopModelList!![k]
                        if (flightBusStopModel.busStopId.toLong() == idA && !wasA) {
                            wasA = true
                            timeFrom = flightBusStopModel.stopTime!!
                        } else if (wasA && flightBusStopModel.busStopId.toLong() == idB) {
                            wasB = true
                            timeTo = flightBusStopModel.stopTime!!
                        }
                        if (wasA && wasB) {
                            wasA = false
                            wasB = false
                            var statusText = ""
                            //TEST
                            var status = 3
                            var notify: NotificationsRestModel? = null
                            sort<NotificationsRestModel>(notificationsRestModel!!.toMutableList()) { o1, o2 ->
                                val second: Int = o2.id
                                second.compareTo(o1.id)
                            }
                            for (notification in notificationsRestModel) {
                                if (notification.flightId != null && notification.flightId!! == flightsModel.id) {
                                    notify = notification
                                    statusText = notification.body!!
                                    if (notification.push_type != null) {
                                        when(notification.push_type){
                                            resources.getString(R.string.topic_bus_deny) -> status = 1
                                            resources.getString(R.string.topic_bus_stops) -> status = 0
                                            resources.getString(R.string.topic_bus_last) -> status =2
                                        }

                                    }
                                }
                                var create: Date
                                if (notify != null) {
                                    try {
                                        val parser = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'",Locale("ru","Ru"))
                                        create = parser.parse(notify.createdAt)
                                        val createCal = Calendar.getInstance()
                                        createCal.time = create
                                        //val time = SimpleDateFormat("HH:mm",Locale("ru","Ru")).parse(timeFrom)
                                        val busTime = Calendar.getInstance()
                                        busTime.timeInMillis = createCal.timeInMillis
                                        busTime[Calendar.HOUR_OF_DAY] = SimpleDateFormat("HH", Locale("ru","RU")).parse(timeFrom).time.toInt()
                                        busTime[Calendar.MINUTE] = SimpleDateFormat("mm", Locale("ru","RU")).parse(timeFrom).time.toInt()
                                        if (createCal.after(busTime))  busTime.add(Calendar.DAY_OF_YEAR, 1)
                                        val now = Calendar.getInstance()
                                        if (now.after(busTime)) {
                                            status = 3
                                        }
                                    } catch (e: ParseException) {
                                        e.printStackTrace()
                                    }
                                    break
                                }
                            }
                            //Пригодится
                            nearestBusModelsList!!.add(
                                NearestBusModel(
                                    timeFrom,
                                    timeTo,
                                    "",
                                    status,
                                    statusText,
                                    flightsModel.isWorkingDaysAvailability,
                                    flightsModel.isWeekendDaysAvailability
                                )
                            )
                            val timeAfter = StringBuilder()
                            try { //Если время рейса больше текущего, то смотрим, в какие дни он ходит, если попадает в нужные дни, то считаем тупо разницу между часами текущего дня
//Если время рейса больше текущего, то смотрим, в какие дни он ходит, если не попадает в нужные дни, смотрим попадает ли следующий день, если да, то считаем разницу между часами сейчас и временем отправления завтра
//Если время рейса больше текущего, то смотрим, в какие дни он ходит, если не попадает в нужные дни, смотрим попадает ли следующий день, если нет, то не добавляем
//Если время рейса меньше текущего, то смотрим, в какие дни он ходит, если завтра попадает в нужные дни, то считаем разницу между сейчас и завтра
//Если время рейса меньше текущего, то смотрим, в какие дни он ходит, если завтра не попадает в нужные дни, то не добавляем
                                //val time = SimpleDateFormat("HH:mm",Locale("ru","Ru")).parse(timeFrom)
                                val time = SimpleDateFormat("HH:mm",Locale("ru","Ru")).parse(timeFrom)
                                val calendar = Calendar.getInstance()
                                calendar.timeInMillis = System.currentTimeMillis()
                                calendar[Calendar.HOUR_OF_DAY] = time.hours
                                calendar[Calendar.MINUTE] = time.minutes
                                val calendarNow = Calendar.getInstance()
                                calendarNow.timeInMillis = System.currentTimeMillis()
                                if (tomorrow) {
                                    calendar.add(Calendar.DATE, 1)
                                }
                                //Проверка дня
                                var isOkDay = false
                                if (isWeekend(calendar[Calendar.DAY_OF_WEEK])) {
                                    if (flightsModel.isWeekendDaysAvailability) {
                                        isOkDay = true
                                    }
                                } else {
                                    if (flightsModel.isWorkingDaysAvailability) {
                                        isOkDay = true
                                    }
                                }
                                //Го все же только на сегодня смотреть?
                                if (isOkDay) { //Если часы и минуты сейчас меньше рейсовых
                                    if (calendarNow.timeInMillis < calendar.timeInMillis) { //будет сегодня
                                        val x = calendar.time
                                        val xy = calendarNow.time
                                        val diff = x.time - xy.time
                                        var diffMinutes = diff / (60 * 1000).toFloat()
                                        val diffHours = diffMinutes / 60
                                        diffMinutes %= 60
                                        timeAfter.append("Через\n")
                                        if (diffHours >= 1) timeAfter.append(diffHours.toInt().toString().plus(" ч. "))
                                        timeAfter.append(diffMinutes.toInt().toString().plus(" мин."))
                                        if (tomorrow) {
                                            nearestBusModelsToShowTomorrow.add(
                                                NearestBusModel(
                                                    timeFrom,
                                                    "-$timeTo",
                                                    timeAfter.toString(),
                                                    status,
                                                    statusText,
                                                    flightsModel.isWorkingDaysAvailability,
                                                    flightsModel.isWeekendDaysAvailability
                                                )
                                            )
                                        } else nearestBusModelsToShow.add(
                                            NearestBusModel(
                                                timeFrom,
                                                "-$timeTo",
                                                timeAfter.toString(),
                                                status,
                                                statusText,
                                                flightsModel.isWorkingDaysAvailability,
                                                flightsModel.isWeekendDaysAvailability
                                            )
                                        )
                                    }
                                    /*else { //будет завтра
                                    }*/
                                }
                            } catch (e: ParseException) {
                                e.printStackTrace()
                            }
                        }
                    }
                }
            }
        }
        return if (tomorrow) {
            if (nearestBusModelsToShowTomorrow.isNotEmpty()) {
                sort<NearestBusModel>(nearestBusModelsToShowTomorrow) {
                        nbm1, nbm2 -> nbm1.from!!.compareTo(nbm2!!.from!!,true)
                }
                nearestBusModelsToShowTomorrow.add(0, NearestBusModel("Завтра", "", "", -1, "", isWorkdays = false, isWeekend = false))
            } else nearestBusModelsToShowTomorrow.add(0, NearestBusModel("См. расписание", "", "", -1, "", isWorkdays = false, isWeekend = false))
            nearestBusModelsToShowTomorrow
        } else {
            if (nearestBusModelsToShow.isNotEmpty()) {
                sort<NearestBusModel>(nearestBusModelsToShow) {
                        nbm1, nbm2 -> nbm1.from!!.compareTo(nbm2!!.from!!,true)
                }
                nearestBusModelsToShow.add(0, NearestBusModel("Сегодня", "", "", -1, "", isWorkdays = false, isWeekend = false))
            }
            nearestBusModelsToShow
        }
    }

    private fun isWeekend(day: Int): Boolean {
        return day == 7 || day == 1
    }

    override fun onResume() {
        super.onResume()
        doRequest()

    }

    companion object{
        fun newInstance(): TransportFragment {

            return TransportFragment()
        }
    }

    inner class PagerAdapter(fm: FragmentManager?) : FragmentPagerAdapter(fm!!, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

        override fun getPageTitle(position: Int): CharSequence? {
            return mTitles[position]
        }

        override fun getItem(position: Int): Fragment {

            return when(position){
                0 -> fragmentA
                else -> fragmentB
            }
        }

        override fun getCount(): Int {
            return mTitles.size
        }
    }

}
