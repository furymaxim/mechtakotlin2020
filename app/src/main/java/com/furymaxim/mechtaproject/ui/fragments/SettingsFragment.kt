package com.furymaxim.mechtaproject.ui.fragments


import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.furymaxim.mechtaproject.R
import com.furymaxim.mechtaproject.adapters.RecyclerViewSettingsAdapter
import com.furymaxim.mechtaproject.models.SettingsModel
import com.furymaxim.mechtaproject.ui.activities.MainActivity
import com.furymaxim.mechtaproject.utils.Utils
import com.google.firebase.messaging.FirebaseMessaging
import kotlinx.android.synthetic.main.fragment_settings.*


class SettingsFragment : Fragment() {

    private var adapter: RecyclerViewSettingsAdapter?= null
    private var settingsList: MutableList<SettingsModel?>? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? { // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_settings, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        Utils.setActionBarTitle(getString(R.string.nav_settings),activity!!)

        (activity as MainActivity).showFlower(true)

        fillSettingsList()

        adapter = RecyclerViewSettingsAdapter(settingsList,context!!)

        setupRecyclerView()
    }

    private fun setupRecyclerView(){
        val linearLayoutManager = LinearLayoutManager(activity)
        val itemDecorator = DividerItemDecoration(context, DividerItemDecoration.VERTICAL)

        itemDecorator.setDrawable(ContextCompat.getDrawable(context!!,R.drawable.recycler_divider)!!)

        recyclerView.layoutManager = linearLayoutManager
        recyclerView.setHasFixedSize(true)
        recyclerView.itemAnimator = DefaultItemAnimator()
        recyclerView.addItemDecoration(itemDecorator)

        recyclerView.adapter = adapter
    }

    private fun fillSettingsList(){
        settingsList = Utils.getSettings(activity)

        val mTitles = resources.getStringArray(R.array.settings_elements)
        val mTopics = resources.getStringArray(R.array.settings_topics)


        if (settingsList == null || mTopics.size != settingsList!!.size) {
            settingsList = ArrayList<SettingsModel>().toMutableList()
            for (i in mTopics.indices) {
                settingsList!!.add(SettingsModel(mTitles[i], mTopics[i], false))
            }

            Utils.setSettings(activity, settingsList)
        }
    }

    companion object{
        fun newInstance(): SettingsFragment {

            return SettingsFragment()

        }


    }

}