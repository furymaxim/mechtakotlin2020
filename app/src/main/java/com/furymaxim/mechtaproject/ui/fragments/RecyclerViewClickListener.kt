package com.furymaxim.mechtaproject.ui.fragments

interface RecyclerViewClickListener {

    fun onRowClicked(position: Int)
}