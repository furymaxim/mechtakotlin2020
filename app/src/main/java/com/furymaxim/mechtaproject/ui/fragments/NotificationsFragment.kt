package com.furymaxim.mechtaproject.ui.fragments


import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.Nullable
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.furymaxim.mechtaproject.R
import com.furymaxim.mechtaproject.adapters.RecyclerViewNotificationAdapter
import com.furymaxim.mechtaproject.api.response_models.NotificationsRestModel
import com.furymaxim.mechtaproject.models.SettingsModel
import com.furymaxim.mechtaproject.ui.activities.MainActivity
import com.furymaxim.mechtaproject.utils.Conf
import com.furymaxim.mechtaproject.utils.Utils
import com.furymaxim.mechtaproject.utils.volley.MyVolley
import com.google.gson.GsonBuilder
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.*


class NotificationsFragment : BaseFragment() {

    //private var mRecyclerView: RecyclerView? = null
    //private var mechtaDatabase: MechtaDatabase? = null
    //private var mRequest: StringRequest? = null
    private val TAG = "NotificationsFragment"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_notifications, container, false)

        mRecyclerView = view.findViewById(R.id.recyclerView)

        Utils.setActionBarTitle(getString(R.string.drawer_item_notifications),activity!!)

        (activity as MainActivity).showFlower(true)

        setupRecyclerView()

        return view
    }

    override fun onActivityCreated(@Nullable savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initMechtaDB()
        requestNotifications()
    }

    private fun requestNotifications(){

        mRequest = StringRequest(Request.Method.POST, Conf.URL_NOTIFICATIONS, Response.Listener { response ->

            if (response != null && response.isNotEmpty()) {
                val gson = GsonBuilder().setDateFormat(NotificationsRestModel.DateFormat).create()
                val notificationsList: List<NotificationsRestModel> = gson.fromJson(response, NotificationsRestModel.Type)

                compositeDisposable.add(
                    Completable.fromAction {
                        mechtaDatabase!!.notificationsDAO().saveAllNotifications(notificationsList)
                    }
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                            { Log.i(TAG, "run: ONCOMPLETE") }
                        )
                        { throwable: Throwable ->
                            Log.i(TAG, "accept: NO")
                            throwable.printStackTrace()
                        }
                )

                val settingsList: List<SettingsModel?>? = Utils.getSettings(context)
                val checkedSettings: MutableList<String?> = ArrayList()
                if (settingsList != null) {
                    for (settings in settingsList) {
                        if (settings!!.checked) {
                            checkedSettings.add(settings.topic)
                        }
                    }
                }

                compositeDisposable.add(mechtaDatabase!!.notificationsDAO().getFilteredNotifications(
                    checkedSettings
                )!!
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnNext {
                        mRecyclerView!!.adapter =
                            RecyclerViewNotificationAdapter(it!!.toMutableList(), context!!)
                    }
                    .doOnError { it.printStackTrace() }
                    .subscribe())
            } else {
                mRequest!!.deliverError(null)
            }

        },Response.ErrorListener {
            it.printStackTrace()
            Toast.makeText(
                context,
                resources.getString(R.string.need_conn),
                Toast.LENGTH_LONG
            ).show()})


        MyVolley.getRequestQueue()!!.add(mRequest)
    }


    override fun setupRecyclerView() {
        val linearLayoutManager = LinearLayoutManager(activity)
        val itemDecorator = DividerItemDecoration(context, DividerItemDecoration.VERTICAL)

        itemDecorator.setDrawable(ContextCompat.getDrawable(context!!, R.drawable.recycler_divider)!!)

        mRecyclerView!!.layoutManager = linearLayoutManager
        mRecyclerView!!.setHasFixedSize(true)
        mRecyclerView!!.itemAnimator = DefaultItemAnimator()
        mRecyclerView!!.addItemDecoration(itemDecorator)
    }



    companion object{
        fun newInstance(): NotificationsFragment {

            return NotificationsFragment()
        }
    }

}
