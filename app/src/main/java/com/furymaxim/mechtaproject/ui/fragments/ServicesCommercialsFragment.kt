package com.furymaxim.mechtaproject.ui.fragments


import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.furymaxim.mechtaproject.R
import com.furymaxim.mechtaproject.adapters.RecyclerViewCommercialsAdapter
import com.furymaxim.mechtaproject.adapters.RecyclerViewPhoneBookAdapter
import com.furymaxim.mechtaproject.api.response_models.CommercialsRestModel
import com.furymaxim.mechtaproject.api.response_models.ServicesRestModel
import com.furymaxim.mechtaproject.ui.activities.MainActivity
import com.furymaxim.mechtaproject.utils.Conf
import com.furymaxim.mechtaproject.utils.TabFragmentInterface
import com.furymaxim.mechtaproject.utils.Utils
import com.furymaxim.mechtaproject.utils.volley.MyVolley
import com.google.gson.Gson
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


class ServicesCommercialsFragment : BaseFragment(), TabFragmentInterface  {

    //private var mRecyclerView: RecyclerView? = null
    //private var mechtaDatabase: MechtaDatabase? = null
    //private var mRequest: StringRequest? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_services_commercials, container, false)

        mRecyclerView = view.findViewById(R.id.recyclerView)

        if (arguments != null) {
            val commercial =
                arguments!!.getBoolean(COMMERCIAL, false)

            if (commercial) {
                Utils.setActionBarTitle(getString(R.string.services),activity!!)
            } else {
                Utils.setActionBarTitle(getString(R.string.title_handbook),activity!!)
            }
        }

        (activity as MainActivity).showFlower(true)

        setupRecyclerView()

        return view
    }

    override fun onResume() {
        super.onResume()
        initMechtaDB()
        requestServicesOrCommercials()
    }

    private fun requestServicesOrCommercials(){
        val commercial = arguments!!.getBoolean(COMMERCIAL)

        if(!commercial){
            compositeDisposable.add(mechtaDatabase!!.servicesDAO().getAllServices()!!
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext {
                    Log.i(
                        TAG,
                        "accept: ACCEPT " + it!!.size
                    )

                    val myList = mutableListOf<ServicesRestModel>()

                    val dispatcher = ServicesRestModel()
                    dispatcher.id = -1
                    dispatcher.phone = resources.getString(R.string.phone_dispatcher)
                    dispatcher.title = resources.getString(R.string.drawer_item_callback)

                    myList.add(dispatcher)

                    for(element in it){
                        myList.add(element!!)
                    }

                    mRecyclerView!!.adapter = RecyclerViewPhoneBookAdapter(myList)
                }
                .doOnError { it.printStackTrace() }
                .subscribe())
        }else{
            compositeDisposable.add(mechtaDatabase!!.commercialsDAO().getAllCommercials()!!
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext {
                    Log.i(
                        TAG,
                        "accept: ACCEPT " + it!!.size
                    )

                    mRecyclerView!!.adapter = RecyclerViewCommercialsAdapter(it)
                }
                .doOnError { it.printStackTrace() }
                .subscribe())
        }

        val request: String = if (commercial) Conf.URL_COMMERCIAL else Conf.URL_SERVICES


        mRequest = StringRequest(Request.Method.GET, request, Response.Listener { response: String? ->
                if (response != null && response.isNotEmpty()) {
                    if (commercial) {
                        val commercialsList: List<CommercialsRestModel> = Gson().fromJson(response, CommercialsRestModel.Type)

                        compositeDisposable.add(
                            Completable.fromAction {
                                mechtaDatabase!!.commercialsDAO().saveAllCommercials(
                                    commercialsList
                                )
                            }
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(
                                    {
                                        Log.i(
                                            TAG,
                                            "run: ONCOMPLETE"
                                        )
                                    }
                                ) { throwable: Throwable ->
                                    Log.i(
                                        TAG,
                                        "accept: NO"
                                    )
                                    throwable.printStackTrace()
                                }
                        )
                    } else {
                        val servicesList: List<ServicesRestModel> =
                            Gson().fromJson(response, ServicesRestModel.Type)
                        compositeDisposable.add(
                            Completable.fromAction {
                                mechtaDatabase!!.servicesDAO().saveAllServices(
                                    servicesList
                                )
                            }
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(
                                    {
                                        Log.i(
                                            TAG,
                                            "run: ONCOMPLETE"
                                        )
                                    }
                                ) { throwable: Throwable ->
                                    Log.i(
                                        TAG,
                                        "accept: NO"
                                    )
                                    throwable.printStackTrace()
                                }
                        )
                    }
                } else {
                    mRequest!!.deliverError(null)
                }
            },
            Response.ErrorListener {
                it.printStackTrace()

                Toast.makeText(
                    context,
                    resources.getString(R.string.need_conn),
                    Toast.LENGTH_LONG
                ).show()
            }
        )

        MyVolley.getRequestQueue()!!.add(mRequest)
    }

    override fun fragmentBecameVisible() {
        //initMechtaDB()
        requestServicesOrCommercials()
    }

    companion object {


        const val COMMERCIAL = "COMMERCIAL"
        const val TAG = "ServicesCommercialsFrom"

        fun newInstance(isCommercial: Boolean): ServicesCommercialsFragment? {
            val fragment = ServicesCommercialsFragment()
            val arguments = Bundle()
            arguments.putBoolean(COMMERCIAL, isCommercial)
            fragment.arguments = arguments
            return fragment
        }
    }

}
