package com.furymaxim.mechtaproject.ui.fragments.tabs.transport_tabs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.furymaxim.mechtaproject.R
import com.furymaxim.mechtaproject.adapters.RecyclerViewScheduleAdapter
import com.furymaxim.mechtaproject.models.ScheduleModel
import com.furymaxim.mechtaproject.utils.TabFragmentInterface

class ScheduleTabFragment : Fragment(), TabFragmentInterface {


    private var mRecyclerView: RecyclerView? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.recycler_view_layout, container, false)
        mRecyclerView = view.findViewById(R.id.recycler_view)
        mRecyclerView!!.setHasFixedSize(true)
        val mLayoutManager: RecyclerView.LayoutManager = LinearLayoutManager(context)
        mRecyclerView!!.layoutManager = mLayoutManager
        return view
    }

    fun showSchedule(scheduleModels: List<ScheduleModel?>?) {
        mRecyclerView!!.adapter = RecyclerViewScheduleAdapter(scheduleModels, context!!)
    }

    override fun fragmentBecameVisible() {}
}