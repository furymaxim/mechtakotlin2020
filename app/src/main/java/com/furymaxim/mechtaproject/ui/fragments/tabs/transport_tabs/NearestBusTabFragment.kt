package com.furymaxim.mechtaproject.ui.fragments.tabs.transport_tabs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.furymaxim.mechtaproject.R
import com.furymaxim.mechtaproject.adapters.RecyclerViewNearestBusAdapter
import com.furymaxim.mechtaproject.models.NearestBusModel
import com.furymaxim.mechtaproject.utils.TabFragmentInterface

class NearestBusTabFragment : Fragment(), TabFragmentInterface {

    private var mRecyclerView: RecyclerView? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.recycler_view_layout, container,false)
        mRecyclerView = view.findViewById(R.id.recycler_view)
        mRecyclerView!!.setHasFixedSize(true)
        val mLayoutManager: RecyclerView.LayoutManager = LinearLayoutManager(context)
        mRecyclerView!!.layoutManager = mLayoutManager
        return view
    }

    fun showNearestBus(nearestBusModelList: List<NearestBusModel?>?) {
        mRecyclerView!!.adapter = RecyclerViewNearestBusAdapter(nearestBusModelList, context!!)
    }

    override fun fragmentBecameVisible() {}
}