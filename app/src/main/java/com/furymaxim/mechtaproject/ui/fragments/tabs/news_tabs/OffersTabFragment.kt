package com.furymaxim.mechtaproject.ui.fragments.tabs.news_tabs

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.furymaxim.mechtaproject.R
import com.furymaxim.mechtaproject.adapters.RecyclerViewOffersAdapter
import com.furymaxim.mechtaproject.api.response_models.OffersRestModel
import com.furymaxim.mechtaproject.ui.fragments.BaseFragment
import com.furymaxim.mechtaproject.utils.Conf
import com.furymaxim.mechtaproject.utils.TabFragmentInterface
import com.furymaxim.mechtaproject.utils.volley.MyVolley
import com.google.gson.Gson
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class OffersTabFragment : BaseFragment(), TabFragmentInterface{

    //private var mRecyclerView: RecyclerView? = null
    //private var mechtaDatabase: MechtaDatabase? = null
    //private var mRequest: StringRequest? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view= inflater.inflate(R.layout.recycler_view_layout, container,false)

        mRecyclerView = view.findViewById(R.id.recycler_view)

        setupRecyclerView()

        return view
    }

    private fun requestOffers(){

        compositeDisposable.add(mechtaDatabase!!.offersDAO().getAllOffers()!!
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnNext {
                Log.i(TAG, "accept: ACCEPT " + it!!.size)
                mRecyclerView!!.adapter = RecyclerViewOffersAdapter(it)
            }
            .doOnError { it.printStackTrace() }
            .subscribe())

        mRequest = StringRequest(
            Request.Method.GET, Conf.URL_OFFERS, Response.Listener { response: String? ->

                if (response != null && response.isNotEmpty()) {
                    val offersList: List<OffersRestModel> = Gson().fromJson(response, OffersRestModel.Type)

                    compositeDisposable.add(
                        Completable.fromAction {
                            mechtaDatabase!!.offersDAO().saveAllOffers(offersList)
                        }
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(
                                {
                                    Log.i(TAG, "run: ONCOMPLETE")
                                }
                            )
                            { throwable: Throwable ->
                                Log.i(TAG, "accept: NO")
                                throwable.printStackTrace()
                            }
                    )
                } else {
                    mRequest!!.deliverError(null)
                }
            },
            Response.ErrorListener {
                it.printStackTrace()

                Toast.makeText(context, getString(R.string.need_conn), Toast.LENGTH_LONG).show()
            }
        )

        MyVolley.getRequestQueue()!!.add(mRequest)


    }

    override fun onResume() {
        super.onResume()
        //mechtaDatabase = MechtaDatabase.getInstance(context!!)
        initMechtaDB()
        requestOffers()
    }

    override fun fragmentBecameVisible() {
        //mechtaDatabase = MechtaDatabase.getInstance(context!!)
        initMechtaDB()
        requestOffers()
    }

    companion object{
        const val TAG = "OffersTabFragment"
    }
}