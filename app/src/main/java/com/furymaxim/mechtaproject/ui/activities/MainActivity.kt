package com.furymaxim.mechtaproject.ui.activities

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.TextUtils
import android.view.Menu
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import com.furymaxim.mechtaproject.R
import com.furymaxim.mechtaproject.ui.fragments.*
import com.furymaxim.mechtaproject.ui.fragments.NotificationsFragment
import com.furymaxim.mechtaproject.ui.navigation_drawer.MenuDrawerItem
import com.furymaxim.mechtaproject.utils.Utils
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.mikepenz.materialdrawer.Drawer
import com.mikepenz.materialdrawer.DrawerBuilder
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var mFragmentNum = 1

    private var result: Drawer? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val toolbar = findViewById<Toolbar>(R.id.toolbar)

        setSupportActionBar(toolbar)
        if (supportActionBar != null) supportActionBar!!.setDisplayHomeAsUpEnabled(true)


        setupDrawer(toolbar)
        setDrawerSelection(savedInstanceState)

    }

    private fun setupDrawer(toolbar: Toolbar){
        result = DrawerBuilder()
            .withActivity(this)
            .withToolbar(toolbar)
            .withSelectedItemByPosition(1)
            .withHeader(R.layout.drawer_header)
            .addDrawerItems(
                MenuDrawerItem().withName(R.string.drawer_item_news).withIcon(
                    R.drawable.ic_alarm_new
                )
                    .withLineColor(this,
                        R.color.new_yellow
                    ),
                MenuDrawerItem().withName(R.string.drawer_item_transport).withIcon(
                    R.drawable.ic_bus_new
                )
                    .withLineColor(this,
                        R.color.new_red
                    ),
                MenuDrawerItem().withName(R.string.services).withIcon(
                    R.drawable.ic_bag_new
                )
                    .withLineColor(this,
                        R.color.new_orange
                    ),
                MenuDrawerItem().withName(R.string.title_handbook).withIcon(
                    R.drawable.ic_note_book
                )
                    .withLineColor(this,
                        R.color.new_violet
                    ),
                MenuDrawerItem().withName(R.string.drawer_item_notifications).withIcon(
                    R.drawable.ic_bell_new
                )
                    .withLineColor(this,
                        R.color.splash_gradient_end
                    ),
                MenuDrawerItem().withName(R.string.drawer_item_feedback).withIcon(
                    R.drawable.ic_star_new
                )
                    .withLineColor(this,
                        R.color.new_green
                    ),
                MenuDrawerItem().withName(R.string.drawer_item_settings).withIcon(
                    R.drawable.ic_settings_new
                )
                    .withLineColor(this,
                        R.color.new_blue
                    )
            )

            .withOnDrawerListener(object: Drawer.OnDrawerListener{

                override fun onDrawerSlide(drawerView: View?, slideOffset: Float) {

                }

                override fun onDrawerClosed(drawerView: View?) {
                }

                override fun onDrawerOpened(drawerView: View?) {

                    Utils.hideKeyboard(this@MainActivity)
                }
            })

            .withOnDrawerItemClickListener { _, position, _ ->
                setFragment(position)
                return@withOnDrawerItemClickListener false
            }
            .build()
    }


    private fun setDrawerSelection(savedInstanceState: Bundle?){
        if (savedInstanceState != null && savedInstanceState.containsKey(FRAGMENT_NUM_KEY)) {
            mFragmentNum = savedInstanceState.getInt(FRAGMENT_NUM_KEY, 0)
        } else {
            val notify = intent.getStringExtra(NOTIFY_VIEW)
            if (!TextUtils.isEmpty(notify) && notify == "true") {
                mFragmentNum = 5
            }
        }

        result!!.setSelectionAtPosition(mFragmentNum)
    }


    private fun setFragment(position: Int) {
        var fr: Fragment? = null
        var tag: String? = null
        when (position) {
            1 -> {
                fr = NewsFragment.newInstance()
                tag = "news"
            }
            2 -> {
                fr = TransportFragment.newInstance()
                tag = "transport"
            }
            3 -> {
                fr = ServicesCommercialsFragment.newInstance(true)
                tag = "services"
            }
            4 -> {
                fr = ServicesCommercialsFragment.newInstance(false)
                tag = "services"
            }
            5 -> {
               fr = NotificationsFragment.newInstance()
                tag = "notifications"
            }
            6 -> {
                try {
                    val intent =
                        Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=ru.mechta.app"))
                    startActivity(intent)
                } catch (e: Exception) {
                    Toast.makeText(
                        this,
                        getString(R.string.google_play_not_found),
                        Toast.LENGTH_SHORT
                    ).show()
                }
                return
            }
            7 -> {
                fr = SettingsFragment.newInstance()
                tag = "settings"
            }
        }
        val backFragment: Fragment? = supportFragmentManager.findFragmentByTag(tag)
        if (backFragment == null || !backFragment.isVisible) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.content_frame, fr!!)
                .commit()
        }
    }

    private fun checkPlayServices(){
        val instance = GoogleApiAvailability.getInstance()
        val resultCode = instance.isGooglePlayServicesAvailable(applicationContext)
        if(resultCode != ConnectionResult.SUCCESS){
            if(instance.isUserResolvableError(resultCode)){
                instance.getErrorDialog(this@MainActivity,resultCode,PLAY_SERVICES_RESOLUTION_REQUEST).show()
            } else{
                Toast.makeText(this, getString(R.string.device_not_supported), Toast.LENGTH_LONG)
                    .show()
                finish()
            }
        }
    }

    override fun onBackPressed() {
        if (result!!.isDrawerOpen) {
            result!!.closeDrawer()
        }
        if (supportFragmentManager.backStackEntryCount == 1) {
            finish()
        } else {
            super.onBackPressed()
        }
    }

    override fun onResume() {
        super.onResume()
        checkPlayServices()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putInt(FRAGMENT_NUM_KEY, mFragmentNum)
        super.onSaveInstanceState(outState)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }

    fun showFlower(visible: Boolean) {
        flower.visibility = if (visible) View.VISIBLE else View.GONE
    }

    companion object {
        const val NOTIFY_VIEW = "notify"
        const val PLAY_SERVICES_RESOLUTION_REQUEST = 9000
        const val FRAGMENT_NUM_KEY = "MainActivity:FragmentNum"
    }


}
