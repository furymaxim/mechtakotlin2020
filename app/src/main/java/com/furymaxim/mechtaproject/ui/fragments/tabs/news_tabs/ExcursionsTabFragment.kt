package com.furymaxim.mechtaproject.ui.fragments.tabs.news_tabs

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.furymaxim.mechtaproject.R
import com.furymaxim.mechtaproject.adapters.RecyclerViewExcursionsAdapter
import com.furymaxim.mechtaproject.api.response_models.ExcursionsRestModel
import com.furymaxim.mechtaproject.ui.fragments.BaseFragment
import com.furymaxim.mechtaproject.utils.Conf
import com.furymaxim.mechtaproject.utils.TabFragmentInterface
import com.furymaxim.mechtaproject.utils.volley.MyVolley
import com.google.gson.Gson
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class ExcursionsTabFragment : BaseFragment(), TabFragmentInterface{

    //private var mRecyclerView: RecyclerView? = null
    //private var mechtaDatabase: MechtaDatabase? = null
    //private var mRequest: StringRequest? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view= inflater.inflate(R.layout.recycler_view_layout, container,false)

        mRecyclerView = view.findViewById(R.id.recycler_view)

        setupRecyclerView()

        return view
    }

    private fun requestIncursions(){

        compositeDisposable.add(mechtaDatabase!!.excursionsDAO().getAllExcursions()!!
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnNext {
                Log.i(TAG, "accept: ACCEPT " + it!!.size)

                mRecyclerView!!.adapter = RecyclerViewExcursionsAdapter(it)
            }
            .doOnError { it.printStackTrace() }
            .subscribe())

        mRequest = StringRequest(Request.Method.GET, Conf.URL_EXCURSIONS, Response.Listener { response: String? ->

            if (response != null && response.isNotEmpty()) {
                val excursionsList: List<ExcursionsRestModel> = Gson().fromJson(response, ExcursionsRestModel.Type)

                compositeDisposable.add(
                    Completable.fromAction {
                        mechtaDatabase!!.excursionsDAO().saveAllExcursions(excursionsList)
                    }
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                            {
                                Log.i(TAG, "run: ONCOMPLETE")
                            }
                        )
                        { throwable: Throwable ->
                            Log.i(TAG, "accept: NO")
                            throwable.printStackTrace()
                        }
                )
            } else {
            mRequest!!.deliverError(null)
        }
                },
            Response.ErrorListener {
                it.printStackTrace()

                Toast.makeText(context, getString(R.string.need_conn), Toast.LENGTH_LONG).show()
            }
        )

        MyVolley.getRequestQueue()!!.add(mRequest)


    }

    override fun onResume() {
        super.onResume()
        initMechtaDB()
        requestIncursions()
    }

    override fun fragmentBecameVisible() {
        initMechtaDB()
        requestIncursions()
    }

    companion object{
        const val TAG = "ExcursionsTabFragment"
    }
}