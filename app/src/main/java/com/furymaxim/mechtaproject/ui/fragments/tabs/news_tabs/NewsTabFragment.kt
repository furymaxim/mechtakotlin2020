package com.furymaxim.mechtaproject.ui.fragments.tabs.news_tabs

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.furymaxim.mechtaproject.R
import com.furymaxim.mechtaproject.adapters.RecyclerViewNewsAdapter
import com.furymaxim.mechtaproject.api.response_models.NewsRestModel
import com.furymaxim.mechtaproject.ui.fragments.BaseFragment
import com.furymaxim.mechtaproject.utils.Conf
import com.furymaxim.mechtaproject.utils.TabFragmentInterface
import com.furymaxim.mechtaproject.utils.volley.MyVolley
import com.google.gson.GsonBuilder
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class NewsTabFragment : BaseFragment(), TabFragmentInterface{

    //private var mRecyclerView: RecyclerView? = null
    //private var mechtaDatabase: MechtaDatabase? = null
    //private var mRequest: StringRequest? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view= inflater.inflate(R.layout.recycler_view_layout, container,false)

        mRecyclerView = view.findViewById(R.id.recycler_view)
        initMechtaDB()

        setupRecyclerView()

        showNews()

        if (networkAvailable()) {
            Thread(Runnable { mechtaDatabase!!.newsDAO().deleteAllNews() })

        }else{
            Toast.makeText(context, getString(R.string.need_conn_for_update), Toast.LENGTH_LONG).show()

        }

        requestNews()

        return view
    }

    /*private fun setupRecyclerView(){

        val linearLayoutManager = LinearLayoutManager(activity)

        mRecyclerView!!.layoutManager = linearLayoutManager
        mRecyclerView!!.setHasFixedSize(true)
    }*/

    private fun showNews(){

        compositeDisposable.add(mechtaDatabase!!.newsDAO().getAllNews()!!
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnNext {
                Log.i(TAG, "accept: ACCEPT " + it!!.size)
                mRecyclerView!!.adapter = RecyclerViewNewsAdapter(it)
            }
            .doOnError { it.printStackTrace() }
            .subscribe())
    }

    private fun requestNews(){

        mRequest = StringRequest(
            Request.Method.GET, Conf.URL_NEWS, Response.Listener { response: String? ->

                if (response != null && response.isNotEmpty()) {
                    val gson = GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").create()
                    val newsList: List<NewsRestModel> = gson.fromJson(response, NewsRestModel.Type)

                    compositeDisposable.add(
                        Completable.fromAction {
                            mechtaDatabase!!.newsDAO().saveAllNews(newsList)
                        }
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(
                                {
                                    Log.i(TAG, "run: ONCOMPLETE")
                                }
                            )
                            { throwable: Throwable ->
                                Log.i(TAG, "accept: NO")
                                throwable.printStackTrace()
                            }
                    )
                } else {
                    mRequest!!.deliverError(null)
                }
            },
            Response.ErrorListener {
                it.printStackTrace()
                Log.d("myLogs",it.toString())
                //Toast.makeText(context, getString(R.string.need_conn), Toast.LENGTH_LONG).show()
            }
        )

        MyVolley.getRequestQueue()!!.add(mRequest)


    }


    override fun fragmentBecameVisible() {
    }

    companion object{
        const val TAG = "NewsTabFragment"
    }
}