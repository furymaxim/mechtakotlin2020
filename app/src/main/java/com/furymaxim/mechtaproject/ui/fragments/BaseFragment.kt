package com.furymaxim.mechtaproject.ui.fragments

import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.toolbox.StringRequest
import com.furymaxim.mechtaproject.utils.database.MechtaDatabase
import io.reactivex.disposables.CompositeDisposable
import java.io.IOException

open class BaseFragment : Fragment() {

    var compositeDisposable = CompositeDisposable()
    var mRecyclerView: RecyclerView? = null
    var mRequest: StringRequest? = null
    var mechtaDatabase: MechtaDatabase? = null


    override fun onDestroyView() {
        super.onDestroyView()
        compositeDisposable.clear()
    }

    open fun setupRecyclerView(){
        val linearLayoutManager = LinearLayoutManager(activity)

        mRecyclerView!!.layoutManager = linearLayoutManager
        mRecyclerView!!.setHasFixedSize(true)
    }

    protected fun initMechtaDB(){
        mechtaDatabase = MechtaDatabase.getInstance(context!!)
    }

    protected fun networkAvailable(): Boolean {

        val runtime = Runtime.getRuntime()
        try {
            val ipProcess = runtime.exec("/system/bin/ping -c 1 8.8.8.8")
            val exitValue = ipProcess.waitFor()
            return (exitValue == 0)
        }
        catch (e: IOException)          { e.printStackTrace(); }
        catch (e:InterruptedException) { e.printStackTrace(); }

        return false
    }
}