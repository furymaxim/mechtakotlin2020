package com.furymaxim.mechtaproject.adapters

import android.content.Context
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.furymaxim.mechtaproject.R
import com.furymaxim.mechtaproject.adapters.RecyclerViewNearestBusAdapter.NearestBusViewHolder
import com.furymaxim.mechtaproject.models.NearestBusModel
import it.sephiroth.android.library.tooltip.Tooltip
import it.sephiroth.android.library.tooltip.Tooltip.ClosePolicy
import it.sephiroth.android.library.tooltip.Tooltip.TooltipView

class RecyclerViewNearestBusAdapter(private val routes: List<NearestBusModel?>?, private val mContext: Context) : RecyclerView.Adapter<NearestBusViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): NearestBusViewHolder {
        val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.recycler_item_nearest_bus, viewGroup, false)
        return NearestBusViewHolder(v)
    }

    override fun onBindViewHolder(viewHolder: NearestBusViewHolder, i: Int) {
        if (routes!![i]!!.to!!.isNotEmpty()) {

            when(i){
                0 -> {
                    viewHolder.setItem(routes[i], ContextCompat.getColor(mContext,R.color.new_green))
                }
                1 -> {
                    viewHolder.setItem(routes[i], ContextCompat.getColor(mContext,R.color.new_text_dark))
                }
                else -> {
                    viewHolder.setItem(routes[i], ContextCompat.getColor(mContext,R.color.new_text_light_darker))
                }
            }
        } else {
            viewHolder.setItem(routes[i], ContextCompat.getColor(mContext,R.color.new_blue))
        }
    }

    override fun getItemCount(): Int {
        return routes!!.size
    }

    inner class NearestBusViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView), View.OnClickListener {
        private var nItem: NearestBusModel? = null
        private val timeFrom: TextView
        private val timeTo: TextView
        private val timeAfter: TextView
        private val badge: ImageView
        private var tooltipView: TooltipView? = null
        fun setItem(item: NearestBusModel?, color: Int) {
            nItem = item
            timeFrom.text = nItem!!.from
            timeTo.text = nItem!!.to
            timeAfter.text = nItem!!.after
            if (color == ContextCompat.getColor(mContext,R.color.new_blue)) timeFrom.setTextSize(
                TypedValue.COMPLEX_UNIT_SP,
                24f
            ) else timeFrom.setTextSize(TypedValue.COMPLEX_UNIT_SP, 32f)
            timeAfter.setTextColor(color)
            tooltipView = when (nItem!!.status) {
                0 -> {
                    badge.setImageDrawable(
                        ContextCompat.getDrawable(
                            mContext,
                            R.drawable.circle_green
                        )
                    )
                    Tooltip.make(
                        mContext,
                        Tooltip.Builder(104)
                            .anchor(badge, Tooltip.Gravity.TOP)
                            .closePolicy(
                                ClosePolicy()
                                    .insidePolicy(true, false)
                                    .outsidePolicy(true, false), 3000
                            )
                            .activateDelay(250)
                            .withStyleId(R.style.MyGreenTooltip)
                            .showDelay(100)
                            .text(nItem!!.text)
                            .maxWidth(1000)
                            .withArrow(true)
                            .build()
                    )
                }
                1 -> {
                    badge.setImageDrawable(
                        ContextCompat.getDrawable(
                            mContext,
                            R.drawable.circle_yellow
                        )
                    )
                    Tooltip.make(
                        mContext,
                        Tooltip.Builder(105)
                            .anchor(badge, Tooltip.Gravity.TOP)
                            .closePolicy(
                                ClosePolicy()
                                    .insidePolicy(true, false)
                                    .outsidePolicy(true, false), 3000
                            )
                            .activateDelay(250)
                            .withStyleId(R.style.MyYellowTooltip)
                            .showDelay(100)
                            .text(nItem!!.text)
                            .maxWidth(1000)
                            .withArrow(true)
                            .build()
                    )
                }
                2 -> {
                    badge.setImageDrawable(
                        ContextCompat.getDrawable(
                            mContext,
                            R.drawable.circle_red
                        )
                    )
                    Tooltip.make(
                        mContext,
                        Tooltip.Builder(106)
                            .anchor(badge, Tooltip.Gravity.TOP)
                            .closePolicy(
                                ClosePolicy()
                                    .insidePolicy(true, false)
                                    .outsidePolicy(true, false), 3000
                            )
                            .activateDelay(250)
                            .withStyleId(R.style.MyRedTooltip)
                            .showDelay(100)
                            .text(nItem!!.text)
                            .maxWidth(1000)
                            .withArrow(true)
                            .build()
                    )
                }
                else -> {
                    badge.setImageDrawable(null)
                    null
                }
            }
        }

        override fun onClick(view: View) {
            if (tooltipView != null) tooltipView!!.show()
        }

        init {
            itemView.setOnClickListener(this)
            timeFrom = itemView.findViewById(R.id.time_from)
            timeTo = itemView.findViewById(R.id.time_to)
            timeAfter = itemView.findViewById(R.id.time_after)
            badge = itemView.findViewById(R.id.image_badge)
        }
    }

}