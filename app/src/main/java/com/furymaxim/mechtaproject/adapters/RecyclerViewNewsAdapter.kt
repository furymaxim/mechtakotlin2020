package com.furymaxim.mechtaproject.adapters

import android.text.SpannableString
import android.text.Spanned
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.furymaxim.mechtaproject.R
import com.furymaxim.mechtaproject.adapters.RecyclerViewNewsAdapter.NewsViewHolder
import com.furymaxim.mechtaproject.api.response_models.NewsRestModel
import com.furymaxim.mechtaproject.ui.fragments.RecyclerViewClickListener
import com.furymaxim.mechtaproject.utils.Conf
import com.furymaxim.mechtaproject.utils.Utils.Companion.openURL
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso.get
import java.lang.Exception

class RecyclerViewNewsAdapter(private val newsList: List<NewsRestModel?>?) : RecyclerView.Adapter<NewsViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): NewsViewHolder {


        val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.recycler_item_news, viewGroup, false)

        return NewsViewHolder(v, object : RecyclerViewClickListener {

            override fun onRowClicked(position: Int) {
                viewGroup.context.startActivity(openURL(newsList!![i]!!.detail_url!!))
            }
        })
    }


    override fun onBindViewHolder(viewHolder: NewsViewHolder, i: Int) {
        viewHolder.setItem(newsList!![i])
    }

    override fun getItemCount(): Int {
        return newsList!!.size
    }

    inner class NewsViewHolder(itemView: View, private val mListener: RecyclerViewClickListener?) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        private var nItem: NewsRestModel? = null
        private val nTitle: TextView
        private val nDate: TextView
        private val nDesc: TextView
        private val nImage: ImageView
        private val progressBar: ProgressBar

        fun setItem(item: NewsRestModel?) {
            nItem = item
            nTitle.text = nItem!!.title
            nDate.text = nItem!!.published_at.toString()
            nDesc.movementMethod = LinkMovementMethod()
            showFull(false, item!!)
            get().load(Conf.URL_MAIN + nItem!!.getPhoto()).into(nImage, object: Callback{
                override fun onSuccess() {
                    progressBar.visibility = View.GONE
                }

                override fun onError(e: Exception?) {
                }
            })
        }

        private fun showFull(isFull: Boolean, item: NewsRestModel) {
            val spannableString: SpannableString
            val offset: Int
            val lengthDesc = item.description!!.length
            if (lengthDesc > 220) {
                if (isFull) {
                    spannableString = SpannableString(
                        itemView.context.getString(
                            R.string.collapse,
                            item.description
                        )
                    )
                    offset = spannableString.length - 8
                } else {
                    spannableString = SpannableString(itemView.context.getString(R.string.expand, item.description!!.substring(0, 200)
                        )
                    )
                    offset = spannableString.length - 13
                }
                spannableString.setSpan(object : ClickableSpan() {
                    override fun onClick(widget: View) {
                        showFull(!isFull, item)
                    }

                    override fun updateDrawState(ds: TextPaint) {
                        super.updateDrawState(ds)
                        ds.color = if (!isFull) ContextCompat.getColor(itemView.context, R.color.new_blue) else ContextCompat.getColor(itemView.context, R.color.new_red)
                        ds.isUnderlineText = false
                    }
                }, offset, spannableString.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
                nDesc.text = spannableString
            } else {
                nDesc.text = item.description
            }
        }

        override fun onClick(view: View) {
            mListener?.onRowClicked(adapterPosition)
        }

        init {
            itemView.setOnClickListener(this)
            nTitle = itemView.findViewById(R.id.newsTitle)
            nDate = itemView.findViewById(R.id.newsDate)
            nDesc = itemView.findViewById(R.id.newsDesc)
            nImage = itemView.findViewById(R.id.newsImage)
            progressBar = itemView.findViewById(R.id.progressBar)
        }
    }

}