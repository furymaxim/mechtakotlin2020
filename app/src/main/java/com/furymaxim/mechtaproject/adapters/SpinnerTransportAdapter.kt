package com.furymaxim.mechtaproject.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.SpinnerAdapter
import android.widget.TextView
import com.furymaxim.mechtaproject.R
import com.furymaxim.mechtaproject.api.response_models.BusStopsRestModel

class SpinnerTransportAdapter(private val mInflater: LayoutInflater, private val mItems: MutableList<BusStopsRestModel?>?) : BaseAdapter(), SpinnerAdapter {

    override fun getCount(): Int {
        return mItems!!.size
    }

    override fun getItem(i: Int): BusStopsRestModel {
        return if (i >= mItems!!.size || i < 0) {
            BusStopsRestModel()
        } else mItems[i]!!
    }

    fun getItems() : List<BusStopsRestModel?>?{
        return mItems
    }

    override fun getItemId(i: Int): Long {
        return mItems!![i]!!.id.toLong()
    }


    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view = mInflater.inflate(R.layout.spinner_item,parent,false)
        (view.findViewById<View>(android.R.id.text1) as TextView).text = mItems!![position]!!.title
        return view
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view = mInflater.inflate(R.layout.spinner_item_dropdown,parent,false)
        (view.findViewById<View>(android.R.id.text1) as TextView).text = mItems!![position]!!.title
        return view
    }

    override fun isEnabled(position: Int): Boolean {
        if (position == 0) {
            return false
        }
        return true
    }
}