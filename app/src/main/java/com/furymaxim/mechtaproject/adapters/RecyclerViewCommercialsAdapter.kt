package com.furymaxim.mechtaproject.adapters

import android.content.Intent
import android.net.Uri
import android.text.TextUtils
import com.furymaxim.mechtaproject.api.response_models.CommercialsRestModel

class RecyclerViewCommercialsAdapter(services: List<CommercialsRestModel?>?) : RecyclerViewServicesBaseAdapter<CommercialsRestModel?>(services!!) {

    override fun setItem(viewHolder: ServicesViewHolder?, position: Int) {
        val model = services[position]!!
        viewHolder!!.sTitle!!.text = model.title
        if (!TextUtils.isEmpty(model.body)) {
            viewHolder.sDesc!!.text = model.body
        }
        viewHolder.phone!!.setOnClickListener {
            val callIntent = Intent(Intent.ACTION_DIAL)
            callIntent.data = Uri.parse("tel:" + services[position]!!.phone)
            viewHolder.phone!!.context.startActivity(callIntent)
        }
    }

    override fun onRowClick(position: Int) {}
}