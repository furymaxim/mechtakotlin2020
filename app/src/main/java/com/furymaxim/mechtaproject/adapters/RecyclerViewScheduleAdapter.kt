package com.furymaxim.mechtaproject.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.furymaxim.mechtaproject.R
import com.furymaxim.mechtaproject.adapters.RecyclerViewScheduleAdapter.ScheduleViewHolder
import com.furymaxim.mechtaproject.models.ScheduleModel
import it.sephiroth.android.library.tooltip.Tooltip
import it.sephiroth.android.library.tooltip.Tooltip.ClosePolicy
import it.sephiroth.android.library.tooltip.Tooltip.TooltipView

class RecyclerViewScheduleAdapter(private val routes: List<ScheduleModel?>?, private val mContext: Context) : RecyclerView.Adapter<ScheduleViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ScheduleViewHolder {
        return if (i == TYPE_ITEM) {
            val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.recycler_item_schedule, viewGroup, false)
            ScheduleViewHolder(v)
        } else {
            val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.recycler_item_star_description, viewGroup, false)
            ScheduleViewHolder(v)
        }
    }

    override fun onBindViewHolder(viewHolder: ScheduleViewHolder, i: Int) {
        if (i != 0) viewHolder.setItem(routes!![i]!!)
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == 0) TYPE_STAR_DESCRIPTION else TYPE_ITEM
    }

    override fun getItemCount(): Int {
        return routes!!.size
    }

    inner class ScheduleViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView), View.OnClickListener {

        private var nItem: ScheduleModel? = null
        private val timeHour: TextView
        private val minutesLayout: LinearLayout

        fun setItem(item: ScheduleModel?) {
            nItem = item
            timeHour.text = nItem!!.hour
            minutesLayout.removeAllViews()
            for (scheduleMinutes in nItem!!.minutes!!) {
                val v = LayoutInflater.from(mContext).inflate(R.layout.recycler_item_minute_schedule, minutesLayout, false)
                val timeMinute = v.findViewById<TextView>(R.id.time_minute)
                timeMinute.text = scheduleMinutes.minute
                val tooltipView: TooltipView?
                when (scheduleMinutes.status) {
                    0 -> {
                        timeMinute.setBackgroundResource(R.drawable.circle_border_green)
                        tooltipView = Tooltip.make(
                            mContext,
                            Tooltip.Builder(101)
                                .anchor(timeMinute, Tooltip.Gravity.TOP)
                                .closePolicy(
                                    ClosePolicy()
                                        .insidePolicy(true, false)
                                        .outsidePolicy(true, false), 3000
                                )
                                .activateDelay(250)
                                .withStyleId(R.style.MyGreenTooltip)
                                .showDelay(100)
                                .text(scheduleMinutes.text)
                                .maxWidth(1000)
                                .withArrow(true)
                                .build()
                        )
                    }
                    1 -> {
                        timeMinute.setBackgroundResource(R.drawable.circle_border_yellow)
                        tooltipView = Tooltip.make(
                            mContext,
                            Tooltip.Builder(102)
                                .anchor(timeMinute, Tooltip.Gravity.TOP)
                                .closePolicy(
                                    ClosePolicy()
                                        .insidePolicy(true, false)
                                        .outsidePolicy(true, false), 3000
                                )
                                .activateDelay(250)
                                .withStyleId(R.style.MyYellowTooltip)
                                .showDelay(100)
                                .text(scheduleMinutes.text)
                                .maxWidth(1000)
                                .withArrow(true)
                                .build()
                        )
                    }
                    2 -> {
                        timeMinute.setBackgroundResource(R.drawable.circle_border_red)
                        tooltipView = Tooltip.make(
                            mContext,
                            Tooltip.Builder(103)
                                .anchor(timeMinute, Tooltip.Gravity.TOP)
                                .closePolicy(
                                    ClosePolicy()
                                        .insidePolicy(true, false)
                                        .outsidePolicy(true, false), 3000
                                )
                                .activateDelay(250)
                                .withStyleId(R.style.MyRedTooltip)
                                .showDelay(100)
                                .text(scheduleMinutes.text)
                                .maxWidth(1000)
                                .withArrow(true)
                                .build()
                        )
                    }
                    else -> {
                        timeMinute.background = null
                        tooltipView = null
                    }
                }
                timeMinute.setOnClickListener { tooltipView?.show() }
                minutesLayout.addView(v)
            }
        }

        override fun onClick(view: View) {}


        init {
            itemView.setOnClickListener(this)
            timeHour = itemView.findViewById(R.id.time_hour)
            minutesLayout = itemView.findViewById(R.id.minutes_layout)
        }
    }

    companion object{
        const val TYPE_STAR_DESCRIPTION = 0
        const val TYPE_ITEM = 1
    }

}