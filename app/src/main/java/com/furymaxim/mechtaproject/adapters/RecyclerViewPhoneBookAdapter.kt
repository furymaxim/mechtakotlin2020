package com.furymaxim.mechtaproject.adapters

import android.content.Intent
import android.net.Uri
import com.furymaxim.mechtaproject.api.response_models.ServicesRestModel

class RecyclerViewPhoneBookAdapter(services: List<ServicesRestModel?>?) : RecyclerViewServicesBaseAdapter<ServicesRestModel?>(services!!) {

    override fun setItem(viewHolder: ServicesViewHolder?, position: Int) {
        val model = services[position]!!
        viewHolder!!.sTitle!!.text = model.title
        viewHolder.sDesc!!.text = model.phone
        viewHolder.phone!!.setOnClickListener {
            val callIntent = Intent(Intent.ACTION_DIAL)
            callIntent.data = Uri.parse("tel:" + services[position]!!.phone)
            viewHolder.phone!!.context.startActivity(callIntent)
        }
    }

    override fun onRowClick(position: Int) {

    }
}