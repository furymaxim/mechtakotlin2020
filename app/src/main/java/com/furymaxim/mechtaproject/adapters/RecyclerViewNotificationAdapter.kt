package com.furymaxim.mechtaproject.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.furymaxim.mechtaproject.R
import com.furymaxim.mechtaproject.adapters.RecyclerViewNotificationAdapter.NotificationsViewHolder
import com.furymaxim.mechtaproject.api.response_models.NotificationsRestModel
import com.furymaxim.mechtaproject.ui.fragments.RecyclerViewClickListener

class RecyclerViewNotificationAdapter(private val notificationsList: MutableList<NotificationsRestModel?>? ,private val context: Context) : RecyclerView.Adapter<NotificationsViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): NotificationsViewHolder {
        val v = LayoutInflater.from(context)
            .inflate(R.layout.recycler_item_notifications, viewGroup, false)

        return NotificationsViewHolder(v, object : RecyclerViewClickListener {
            override fun onRowClicked(position: Int) {
                //mContext.startActivity(Utils.openURL(services.get(i).getDetailUrl()));
                Toast.makeText(context,"Clicked at $position item",Toast.LENGTH_SHORT).show()
            }

        })
    }

    override fun onBindViewHolder(viewHolder: NotificationsViewHolder, position: Int) {
        viewHolder.setItem(notificationsList!![position]!!)
    }

    override fun getItemCount(): Int {
        return notificationsList!!.size
    }

    inner class NotificationsViewHolder(itemView: View, private val mListener: RecyclerViewClickListener?) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        private var nItem: NotificationsRestModel? = null
        private val nText: TextView
        private val nTime: TextView
        private val nImage: ImageView

        fun setItem(item: NotificationsRestModel) {
            nItem = item
            nText.text = nItem!!.body
            nTime.text = nItem!!.published_at.toString()
            val context = nImage.context
            when(item.push_type){
                context.getString(R.string.topic_bus_deny) -> nImage.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_delay))
                context.getString(R.string.topic_bus_stops) -> nImage.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_clock_new))
                context.getString(R.string.topic_bus_last) -> nImage.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_decline))
                context.getString(R.string.topic_common) -> nImage.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_common))
                context.getString(R.string.topic_stock) -> nImage.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_stock))
            }
        }

        override fun onClick(view: View) {
            mListener?.onRowClicked(adapterPosition)
        }

        init {
            itemView.setOnClickListener(this)
            nText = itemView.findViewById(R.id.notifText)
            nTime = itemView.findViewById(R.id.notifDate)
            nImage = itemView.findViewById(R.id.notifImage)
        }
    }


}