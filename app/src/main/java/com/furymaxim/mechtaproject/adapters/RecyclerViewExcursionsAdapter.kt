package com.furymaxim.mechtaproject.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.furymaxim.mechtaproject.R
import com.furymaxim.mechtaproject.adapters.RecyclerViewExcursionsAdapter.ExcursionsViewHolder
import com.furymaxim.mechtaproject.api.response_models.ExcursionsRestModel
import com.furymaxim.mechtaproject.ui.fragments.RecyclerViewClickListener
import com.furymaxim.mechtaproject.utils.Utils.Companion.openURL

class RecyclerViewExcursionsAdapter(private val excursions: List<ExcursionsRestModel?>?) : RecyclerView.Adapter<ExcursionsViewHolder>() {


    override fun onCreateViewHolder(viewGroup: ViewGroup, position: Int): ExcursionsViewHolder {
        val view = LayoutInflater.from(viewGroup.context).inflate(R.layout.recycler_item_excursions, viewGroup, false)

        return ExcursionsViewHolder(view, object : RecyclerViewClickListener {

            override fun onRowClicked(position: Int) {
                view.context.startActivity(
                    openURL(
                        excursions!![position]!!.detail_url!!
                    )
                )
            }
        })
    }

    override fun onBindViewHolder(viewHolder: ExcursionsViewHolder, position: Int) {
        viewHolder.setItem(excursions!![position])
    }

    override fun getItemCount(): Int {
        return excursions!!.size
    }

    inner class ExcursionsViewHolder(itemView: View, private val mListener: RecyclerViewClickListener?) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        private var eItem: ExcursionsRestModel? = null
        private val eTitle: TextView
        private val eDesc: TextView

        fun setItem(item: ExcursionsRestModel?) {
            eItem = item
            eTitle.text = eItem!!.title
            eDesc.text = eItem!!.description
        }

        override fun onClick(view: View) {
            mListener?.onRowClicked(adapterPosition)
        }

        init {
            itemView.setOnClickListener(this)
            eTitle = itemView.findViewById(R.id.excursionsTitle)
            eDesc = itemView.findViewById(R.id.excursionsDesc)
        }
    }

}