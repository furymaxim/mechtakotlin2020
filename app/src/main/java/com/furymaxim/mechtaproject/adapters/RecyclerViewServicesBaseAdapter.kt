package com.furymaxim.mechtaproject.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.furymaxim.mechtaproject.R
import com.furymaxim.mechtaproject.adapters.RecyclerViewServicesBaseAdapter.ServicesViewHolder
import com.furymaxim.mechtaproject.api.response_models.ServicesRestModel
import com.furymaxim.mechtaproject.ui.fragments.RecyclerViewClickListener

abstract class RecyclerViewServicesBaseAdapter<T>(protected var services: List<T>) : RecyclerView.Adapter<ServicesViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ServicesViewHolder {
        val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.recycler_item_services, viewGroup, false)

        return ServicesViewHolder(v, object : RecyclerViewClickListener {
            override fun onRowClicked(position: Int) {
                onRowClick(position)
            }
        })
    }

    override fun onBindViewHolder(viewHolder: ServicesViewHolder, i: Int) {
        setItem(viewHolder, i)
    }

    protected abstract fun setItem(viewHolder: ServicesViewHolder?, position: Int)
    override fun getItemCount(): Int {
        return services.size
    }

    abstract fun onRowClick(position: Int)



    class ServicesViewHolder(itemView: View, private val mListener: RecyclerViewClickListener?) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        var sTitle: TextView? = null
        var sDesc: TextView? = null
        var phone: ImageButton? = null
        private var sItem: ServicesRestModel? = null

        fun setItem(item: ServicesRestModel?) {
            sItem = item
            sTitle!!.text = sItem!!.title
        }

        override fun onClick(view: View) {
            mListener?.onRowClicked(adapterPosition)
        }

        init {
            itemView.setOnClickListener(this)
            sTitle = itemView.findViewById(R.id.servicesTitle)
            sDesc = itemView.findViewById(R.id.servicesDesc)
            phone = itemView.findViewById(R.id.servicePhone)
        }
    }

}