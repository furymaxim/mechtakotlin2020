package com.furymaxim.mechtaproject.adapters

import android.text.SpannableString
import android.text.Spanned
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.furymaxim.mechtaproject.R
import com.furymaxim.mechtaproject.adapters.RecyclerViewOffersAdapter.OffersViewHolder
import com.furymaxim.mechtaproject.api.response_models.OffersRestModel
import com.furymaxim.mechtaproject.ui.fragments.RecyclerViewClickListener
import com.furymaxim.mechtaproject.utils.Utils.Companion.openURL

class RecyclerViewOffersAdapter(private val offers: List<OffersRestModel?>?) : RecyclerView.Adapter<OffersViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, position: Int): OffersViewHolder {
        val view = LayoutInflater.from(viewGroup.context).inflate(R.layout.recycler_item_offers, viewGroup, false)

        return OffersViewHolder(view, object : RecyclerViewClickListener {

            override fun onRowClicked(position: Int) {
                view.context.startActivity(openURL(offers!![position]!!.detail_url!!))
            }
        })
    }

    override fun onBindViewHolder(viewHolder: OffersViewHolder, position: Int) {
        viewHolder.setItem(offers!![position])
    }

    override fun getItemCount(): Int {
        return offers!!.size
    }

    inner class OffersViewHolder(itemView: View, private val mListener: RecyclerViewClickListener?) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        private var oItem: OffersRestModel? = null
        private val oTitle: TextView
        private val oDesc: TextView

        fun setItem(item: OffersRestModel?) {
            oItem = item
            oTitle.text = oItem!!.title
            showFull(false, item!!)
            oDesc.movementMethod = LinkMovementMethod()
        }

        private fun showFull(isFull: Boolean, item: OffersRestModel) {
            val spannableString: SpannableString
            val offset: Int
            val lengthDesc = item.description!!.length
            if (lengthDesc > 220) {
                if (isFull) {
                    spannableString = SpannableString(
                        itemView.context.getString(
                            R.string.collapse,
                            item.description
                        )
                    )
                    offset = spannableString.length - 8
                } else {
                    spannableString = SpannableString(
                        itemView.context.getString(
                            R.string.expand,
                            item.description!!.substring(0, 200)
                        )
                    )
                    offset = spannableString.length - 13
                }
                spannableString.setSpan(object : ClickableSpan() {
                    override fun onClick(widget: View) {
                        showFull(!isFull, item)
                    }

                    override fun updateDrawState(ds: TextPaint) {
                        super.updateDrawState(ds)
                        ds.color = if (!isFull) ContextCompat.getColor(
                            itemView.context,
                            R.color.new_blue
                        ) else ContextCompat.getColor(itemView.context, R.color.new_red)
                        ds.isUnderlineText = false
                    }
                }, offset, spannableString.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
                oDesc.text = spannableString
            } else {
                oDesc.text = item.description
            }
        }

        override fun onClick(view: View) {
            mListener?.onRowClicked(adapterPosition)
        }

        init {
            itemView.setOnClickListener(this)
            oTitle = itemView.findViewById(R.id.offersTitle)
            oDesc = itemView.findViewById(R.id.offersDesc)
        }
    }

}