package com.furymaxim.mechtaproject.fcm

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.furymaxim.mechtaproject.R
import com.furymaxim.mechtaproject.ui.activities.MainActivity
import com.furymaxim.mechtaproject.utils.Conf
import com.furymaxim.mechtaproject.utils.volley.MyVolley
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import java.util.*


class MyFireBaseMessagingService : FirebaseMessagingService() {

    private var token: String? = null

    override fun onNewToken(s: String) {
        super.onNewToken(s)
        sendRegistrationIdToBackend()
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        for ((key, value) in remoteMessage.data) {
            Log.d(key, value)
        }
        val message = remoteMessage.data["message"]
        val notify = remoteMessage.data[MainActivity.NOTIFY_VIEW]
        sendNotification(message, notify)
    }

    private fun sendNotification(msg: String?, notify: String?) {
        val mNotificationManager =
            this.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val intent = Intent(this, MainActivity::class.java)
        intent.putExtra(MainActivity.NOTIFY_VIEW, notify)
        val contentIntent = PendingIntent.getActivity(
            this, 0,
            intent, PendingIntent.FLAG_UPDATE_CURRENT
                    or PendingIntent.FLAG_ONE_SHOT
        )
        val vibrate = longArrayOf(0, 100, 200, 300, 300, 400, 400)

        var mBuilder = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val notificationChannel =
                NotificationChannel("ID", "Name", importance)
            mNotificationManager.createNotificationChannel(notificationChannel)
            NotificationCompat.Builder(
                applicationContext,
                notificationChannel.id
            )
        } else {
            NotificationCompat.Builder(applicationContext)
        }

         mBuilder = mBuilder
                .setAutoCancel(true)
                .setSound(Uri.parse("content://settings/system/notification_sound"))
                .setVibrate(vibrate)
                .setLights(Color.BLUE, 500, 500)
                .setSmallIcon(R.drawable.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(resources, R.drawable.ic_launcher))
                .setContentTitle(getString(R.string.app_name))
                .setStyle(NotificationCompat.BigTextStyle().bigText(msg))
                .setContentText(msg)
        mBuilder.setContentIntent(contentIntent)
        val time = Date().time
        val tmpStr = time.toString()
        val last4Str = tmpStr.substring(tmpStr.length - 5)
        val notificationId = Integer.valueOf(last4Str)
        mNotificationManager.notify(notificationId, mBuilder.build())
    }

    private fun sendRegistrationIdToBackend() {
        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener { instanceIdResult ->
            token = instanceIdResult.token
            Log.e("newToken", token)
        }

        if (token == null) return

        val myReq: StringRequest = object : StringRequest(
            Method.POST,
            Conf.URL_FCM_SERVER,
            Response.Listener { },
            null
        ) {
            override fun getParams(): Map<String, String> {
                val params: MutableMap<String, String> =
                    HashMap()
                params["token"] = token!!
                return params
            }

            override fun getHeaders(): Map<String, String> {
                val params: MutableMap<String, String> =
                    HashMap()
                params["Content-Type"] = "application/x-www-form-urlencoded"
                return params
            }
        }
        MyVolley.getRequestQueue()!!.add(myReq)
    }
}