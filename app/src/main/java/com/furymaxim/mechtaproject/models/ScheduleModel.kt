package com.furymaxim.mechtaproject.models

/**
 * Created by admin on 25.02.2017.
 */
class ScheduleModel {
    var hour: String? = null
    var minutes: List<ScheduleMinutesModel>? = null

    constructor() {}
    constructor(hour: String?, minutes: List<ScheduleMinutesModel>?) {
        this.hour = hour
        this.minutes = minutes
    }

}