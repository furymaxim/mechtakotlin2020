package com.furymaxim.mechtaproject.models

class ScheduleMinutesModel {

    var minute: String? = null
    var status = 0
    var text: String? = null

    constructor() {}
    constructor(minute: String?, status: Int, text: String?) {
        this.minute = minute
        this.status = status
        this.text = text
    }

}