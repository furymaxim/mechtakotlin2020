package com.furymaxim.mechtaproject.models


class NearestBusModel {
    var from: String? = null
    var to: String? = null
    var after: String? = null
    var status = 0
    var text: String? = null
    var isWeekend = false
    var isWorkdays = false

    constructor() {}
    constructor(
        from: String?,
        to: String?,
        after: String?,
        status: Int,
        text: String?,
        isWorkdays: Boolean,
        isWeekend: Boolean
    ) {
        this.from = from
        this.to = to
        this.after = after
        this.status = status
        this.text = text
        this.isWeekend = isWeekend
        this.isWorkdays = isWorkdays
    }

}