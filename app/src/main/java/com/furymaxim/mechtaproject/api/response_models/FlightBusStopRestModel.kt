package com.furymaxim.mechtaproject.api.response_models

import com.google.gson.annotations.SerializedName

class FlightBusStopRestModel {
    @SerializedName("bus_stop_id")
    var busStopId = 0
    @SerializedName("stop_time")
    var stopTime: String? = null

}