package com.furymaxim.mechtaproject.api.response_models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import com.google.gson.reflect.TypeToken

@Entity
class ExcursionsRestModel {

    @SerializedName("id")
    @PrimaryKey
    var id = 0
    @SerializedName("start_date")
    var start_date: String? = null
    @SerializedName("title")
    var title: String? = null
    @SerializedName("detail_url")
    var detail_url: String? = null
    @SerializedName("description")
    var description: String? = null
    @SerializedName("published_at")
    var published_at: String? = null
    @SerializedName("photo")
    var photo: String? = null

    companion object {
        var Type =
            object : TypeToken<List<ExcursionsRestModel?>?>() {}.type
    }
}