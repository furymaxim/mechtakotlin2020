package com.furymaxim.mechtaproject.api.response_models

import com.google.gson.annotations.SerializedName

class FlightsRestModel {
    @SerializedName("id")
    var id = 0
    @SerializedName("express")
    var isExpress = false
    @SerializedName("weekend_days_aAvailability")
    var isWeekendDaysAvailability = false
    @SerializedName("working_days_availability")
    var isWorkingDaysAvailability = false
    @SerializedName("bus_stops")
    var flightBusStopModelList: List<FlightBusStopRestModel>? = null

}