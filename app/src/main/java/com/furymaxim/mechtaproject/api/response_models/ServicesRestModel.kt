package com.furymaxim.mechtaproject.api.response_models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import com.google.gson.reflect.TypeToken

@Entity
class ServicesRestModel {

    @SerializedName("id")
    @PrimaryKey
    var id = 0
    @SerializedName("title")
    var title: String? = null
    @SerializedName("phone")
    var phone: String? = null
    @SerializedName("published_at")
    var publishedAt: String? = null

    companion object {
        var Type =
            object : TypeToken<List<ServicesRestModel?>?>() {}.type
    }
}