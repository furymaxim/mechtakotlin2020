package com.furymaxim.mechtaproject.api.response_models

import com.google.gson.annotations.SerializedName
import com.google.gson.reflect.TypeToken

class BusStopsRestModel {
    @SerializedName("id")
    var id = 0
    @SerializedName("title")
    var title: String? = null
    @SerializedName("latitude")
    var latitude: String? = null
    @SerializedName("longitude")
    var longitude: String? = null

    constructor() {}
    constructor(
        id: Int,
        title: String?,
        latitude: String?,
        longitude: String?
    ) {
        this.id = id
        this.title = title
        this.latitude = latitude
        this.longitude = longitude
    }

    companion object {
        var Type =
            object : TypeToken<List<BusStopsRestModel?>?>() {}.type
    }
}