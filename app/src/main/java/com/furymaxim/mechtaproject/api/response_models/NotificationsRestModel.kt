package com.furymaxim.mechtaproject.api.response_models

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.furymaxim.mechtaproject.utils.DateConverter
import com.google.gson.annotations.SerializedName
import com.google.gson.reflect.TypeToken
import java.util.*

@Entity
class NotificationsRestModel {

    @SerializedName("id")
    @PrimaryKey
    var id = 0
    @SerializedName("published_at")
    @TypeConverters(DateConverter::class)
    var published_at: Date? = null
    @SerializedName("created_at")
    var createdAt: String? = null
    @SerializedName("push_type")
    var push_type: String? = null
    @SerializedName("body")
    var body: String? = null
    @SerializedName("bus_route_id")
    var busRouteId: Int? = null
    @SerializedName("flight_id")
    var flightId: Int? = null

    companion object {
        val DateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        val Type =
            object : TypeToken<List<NotificationsRestModel?>?>() {}.type
    }
}