package com.furymaxim.mechtaproject.api.response_models

import android.util.Log
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.furymaxim.mechtaproject.utils.DateConverter
import com.google.gson.annotations.SerializedName
import com.google.gson.reflect.TypeToken
import java.util.*

@Entity
class NewsRestModel {

    @PrimaryKey
    @SerializedName("id")
    var id = 0
    @TypeConverters(DateConverter::class)
    @SerializedName("published_at")
    var published_at: Date? = null
    @TypeConverters(DateConverter::class)
    @SerializedName("updated_at")
    var updatedAt: Date? = null
    @SerializedName("title")
    var title: String? = null
    @SerializedName("detail_url")
    var detail_url: String? = null
    @SerializedName("description")
    var description: String? = null
    @SerializedName("photo")
    private var photo: String? = null

    fun getPhoto(): String? {
        if (photo != null) {
            Log.i(TAG, "getPhoto: URI: $photo")
        }
        return photo
    }

    fun setPhoto(photo: String?) {
        this.photo = photo
    }

    override fun toString(): String {
        return "NewsRestModel{" +
                "id=" + id + '\n' +
                ", published_at=" + published_at + '\n' +
                ", title='" + title + '\n' +
                ", detail_url='" + detail_url + '\n' +
                ", description='" + description + '\n' +
                ", photo='" + photo + '\n' +
                ", updatedAt=" + updatedAt + '\n' +
                '}'
    }

    companion object {

        private const val TAG = "TAG"
        var Type =
            object : TypeToken<List<NewsRestModel?>?>() {}.type
    }
}