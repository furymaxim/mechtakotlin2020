package com.furymaxim.mechtaproject.api.response_models

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.furymaxim.mechtaproject.utils.DateConverter
import com.google.gson.annotations.SerializedName
import com.google.gson.reflect.TypeToken
import java.util.*

@Entity
class OffersRestModel {

    @PrimaryKey
    @SerializedName("id")
    var id = 0
    @SerializedName("title")
    var title: String? = null
    @SerializedName("detail_url")
    var detail_url: String? = null
    @SerializedName("description")
    var description: String? = null
    @SerializedName("published_at")
    var published_at: String? = null
    @SerializedName("photo")
    var photo: String? = null
    @TypeConverters(DateConverter::class)
    @SerializedName("updated_at")
    var updatedAt: Date? = null

    companion object {
        var Type =
            object : TypeToken<List<OffersRestModel?>?>() {}.type
    }
}