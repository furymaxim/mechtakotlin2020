package com.furymaxim.mechtaproject.api.response_models

import com.google.gson.annotations.SerializedName
import com.google.gson.reflect.TypeToken

class BusRoutesRestModel {

    @SerializedName("id")
    var id = 0
    @SerializedName("name")
    var name: String? = null
    @SerializedName("flights")
    var flights: List<FlightsRestModel>? = null
    @SerializedName("notifications")
    var notifications: List<NotificationsRestModel>? = null

    companion object {
        var Type =
            object : TypeToken<List<BusRoutesRestModel?>?>() {}.type
    }
}